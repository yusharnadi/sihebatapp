import React from 'react';
import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import colors from './src/configs/colors';
import {store} from './src/redux';
import Router from './src/Router';
import SplashScreen from 'react-native-splash-screen';
const App = function () {
  React.useEffect(() => {
    SplashScreen.hide();
  });
  return (
    <Provider store={store}>
      <Router />
      <StatusBar backgroundColor={colors.primary} />
    </Provider>
  );
};

export default App;
