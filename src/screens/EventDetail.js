import axios from 'axios';
import React from 'react';
import {
  View,
  Image,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  ScrollView,
} from 'react-native';
import ImageModal from 'react-native-image-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../configs/colors';
import url from '../configs/url';

export default function EventDetail({navigation, route}) {
  const {id} = route.params;

  const [data, setData] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(false);

  const fetchData = () => {
    setIsLoading(true);
    axios
      .get(`${url.api}/kiosk/event/${id}`, {headers: {token: 'secret123'}})
      .then(result => {
        console.log(result.data.data);
        setData(result.data.data);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
        setIsLoading(false);
      });
  };

  React.useEffect(() => {
    fetchData();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {/* <Image style={styles.img} source={{uri: data.image_path}} /> */}
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{uri: data.image_path}}
          imageBackgroundColor="#fff"
          style={{
            height: 200,
            width: Dimensions.get('screen').width,
          }}
        />
        <View style={styles.whiteWrap}>
          <View style={styles.itemWrap}>
            <View style={styles.rowIcon}>
              <Icon name="map-marker" size={12} color={colors.primary} />
              <Text style={styles.txtItem}>{data.tempat}</Text>
            </View>
            <View style={styles.rowIcon}>
              <Icon name="ticket" size={12} color={colors.primary} />
              <Text style={styles.txtItem}>{data.htm}</Text>
            </View>
          </View>
          <View style={styles.itemWrap}>
            <View style={styles.rowIcon}>
              <Icon name="calendar" size={9} color={colors.primary} />
              <Text style={styles.txtItem}>{data.date}</Text>
            </View>
            <View style={styles.rowIcon}>
              <Icon name="tags" size={12} color={colors.primary} />
              <Text style={styles.txtItem}>{data.kategori}</Text>
            </View>
          </View>
          <View style={styles.itemWrap}>
            <View style={styles.rowIcon}>
              <Icon name="clock-o" size={12} color={colors.primary} />
              <Text style={styles.txtItem}>{data.time}</Text>
            </View>
          </View>
        </View>
        <View style={styles.whiteWrap}>
          <Text style={styles.txtJudul}>PENYELENGGARA</Text>
          <View style={styles.rowIconsce}>
            <Icon name="users" size={12} color={colors.primary} />
            <Text style={styles.txtItem}>{data.penyelenggara}</Text>
          </View>
          <View style={styles.rowIconsce}>
            <Icon name="phone" size={14} color={colors.primary} />
            <Text style={styles.txtItem}>{data.kontak_hp}</Text>
          </View>
          <View style={styles.rowIconsce}>
            <Icon name="facebook" size={14} color={colors.primary} />
            <Text style={styles.txtItem}>{data.facebook}</Text>
          </View>
          <View style={styles.rowIconsce}>
            <Icon name="instagram" size={14} color={colors.primary} />
            <Text style={styles.txtItem}>{data.instagram}</Text>
          </View>
        </View>
        <View style={styles.whiteWrap}>
          <Text style={styles.txtJudul}>DETAIL ACARA</Text>
          <Text style={styles.txtIsi}>{data.deskripsi}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDDD',
  },
  img: {
    height: (50 / 100) * Dimensions.get('screen').width,
    // height: '40%',
    width: '100%',
    marginBottom: 8,
    resizeMode: 'cover',
  },
  whiteWrap: {
    backgroundColor: '#fff',
    padding: 16,
    marginBottom: 8,
  },
  itemWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 4,
  },
  rowIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    width: (Dimensions.get('screen').width - 60) / 2,
  },
  rowIconsce: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
  },
  txtItem: {
    marginLeft: 8,
    // fontSize: 12,
    color: '#000',
  },
  txtJudul: {
    fontWeight: '700',
    marginBottom: 8,
    color: '#000',
  },
  txtIsi: {
    color: '#000',
    textAlign: 'justify',
  },
});
