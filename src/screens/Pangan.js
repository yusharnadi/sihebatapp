import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Pangan({navigation}) {
  return (
    <View style={{backgroundColor: '#fff', flex: 1, paddingHorizontal: 16}}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Beringin')}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <Text style={{fontWeight: '400', fontSize: 18}}>Pasar Beringin</Text>
        {/* <Text style={{fontWeight: '700', fontSize: 16}}>=></Text> */}
        <Icon name="arrow-right" size={22} />
      </TouchableOpacity>
      <View
        style={{
          borderTopWidth: 0.5,
          borderColor: '#000',
          marginTop: 10,
        }}></View>
      <TouchableOpacity
        onPress={() => navigation.navigate('Alianyang')}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 20,
        }}>
        <Text style={{fontWeight: '400', fontSize: 18}}>Pasar Alianyang</Text>
        {/* <Text style={{fontWeight: '700', fontSize: 16}}>=></Text> */}
        <Icon name="arrow-right" size={22} />
      </TouchableOpacity>
      <View
        style={{
          borderTopWidth: 0.5,
          borderColor: '#000',
          marginTop: 10,
        }}></View>
    </View>
  );
}
