import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import colors from '../configs/colors';

export default function Splash({navigation}) {
  const [isLoading, setIsLoading] = React.useState(true);

  // !isLoading ? navigation.navigate('Home') : null;

  // React.useEffect(() => {
  //   setInterval(function () {
  //     setIsLoading(false);
  //   }, 5000);
  // });
  return (
    <View style={styles.rootContainer}>
      <Image source={require('../assets/images/logo-utama.png')} />
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    backgroundColor: colors.white,
  },
});
