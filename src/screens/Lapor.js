/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  Button,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../configs/colors';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Picker} from '@react-native-picker/picker';
import FormData from 'form-data';
import axios from 'axios';
import url from '../configs/url';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Lapor({route, navigation}) {
  const [image, SetImage] = React.useState({
    name: null,
    type: null,
    fileUri: null,
  });

  const authData = useSelector(state => state.AuthReducer);
  const [deskripsi, setDeskripsi] = React.useState('');
  const [alamat, setAlamat] = React.useState('');
  const [kelurahan, setKelurahan] = React.useState('');

  const [isLoading, setIsLoading] = React.useState(false);

  const [kelurahanList, setKelurahanList] = React.useState([]);
  const dispatch = useDispatch();

  React.useEffect(() => {
    fetchKelurahan();
  }, []);

  const fetchKelurahan = () => {
    axios
      .get(`${url.api}/kelurahan`, {
        headers: {Accept: 'application/json', token: 'secret123'},
      })
      .then(result => {
        // console.log(result.data);
        setKelurahanList(result.data.data);
      })
      .catch(err => console.log(err));
  };

  const {id, nama} = route.params;

  const handleSubmit = () => {
    if (image.fileUri === null) {
      Alert.alert('Gambar Laporan gak boleh kosong ya.');
      return;
    }

    if (alamat === '') {
      Alert.alert('Alamat Laporan gak boleh kosong ya.');
      return;
    }

    if (deskripsi === '') {
      Alert.alert('Isi Laporan gak boleh kosong ya.');
      return;
    }

    if (kelurahan === '') {
      Alert.alert('Kelurahan juga gak boleh kosong ya.');
      return;
    }

    setIsLoading(true);

    const form = new FormData();
    form.append('deskripsi_laporan', deskripsi);
    form.append('alamat_laporan', alamat);
    form.append('id_kategori', id);
    form.append('id_kelurahan', kelurahan);
    form.append('image', {
      name: image.name,
      type: image.type,
      uri:
        Platform.OS === 'android'
          ? image.fileUri
          : image.fileUri.replace('file://', ''),
    });

    const headers = {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + authData.user_data.access_token,
    };

    axios
      .post(`${url.api}/laporan`, form, {
        headers: headers,
      })
      .then(result => {
        console.log('success ' + result.data.success);
        setIsLoading(false);
        setAlamat('');
        setKelurahan('');
        setDeskripsi('');
        SetImage({
          name: null,
          type: null,
          fileUri: null,
        });
        Alert.alert('Laporan berhasil dikirim.');
        navigation.navigate('Home');
      })
      .catch(err => {
        if (err.response) {
          if (err.response.status === 401) {
            AsyncStorage.multiRemove(['user_data', 'fb_data'])
              .then(data => {
                dispatch({type: 'SET_AUTH_LOGOUT'});
                console.log('Local storage was removed.');
              })
              .catch(err => console.log('Gagal remove localstorage ' + err));
          }
        } else {
          Alert.alert('Maaf, Error menghubungi server.');
        }
        console.log(err);
        Alert.alert('Error, Ukuran Gambar maksimal 2 Mb');
        setIsLoading(false);
      });
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        granted === PermissionsAndroid.RESULTS.GRANTED;
        console.log(granted);
        return granted;
      } catch (err) {
        console.warn(err);
      }
      return false;
    } else {
      return true;
    }
  };

  const handleGalery = async () => {
    let isStoragePermitted = await requestExternalWritePermission();
    const options = {
      title: 'Select Image',
      saveToPhotos: true,
      maxWidth: 1280,
      maxHeight: 720,
    };
    if (isStoragePermitted) {
      launchImageLibrary(options, function (response) {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          console.log('response', JSON.stringify(response));
          SetImage({
            name: response.assets[0].fileName,
            type: response.assets[0].type,
            fileUri: response.assets[0].uri,
          });
        }
      });
    }
  };

  const handleUpload = () => {
    const options = {
      title: 'Select Image',
      saveToPhotos: true,
      maxWidth: 1280,
      maxHeight: 720,
    };

    launchCamera(options, function (response) {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        console.log('response', JSON.stringify(response));
        SetImage({
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          fileUri: response.assets[0].uri,
        });
      }
    });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView style={styles.container}>
        {/* <View style={styles.imgWrapper}></View> */}
        <Image
          source={{
            uri: image.fileUri,
          }}
          style={styles.imgWrapper}
          resizeMode="contain"
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity style={styles.btnUpload} onPress={handleUpload}>
            <Icon name="camera" size={16} color={colors.white} />
            <Text style={styles.btnText}>Buka Kamera</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnUpload} onPress={handleGalery}>
            <Icon name="image" size={16} color={colors.white} />
            <Text style={styles.btnText}>Ambil di Galeri</Text>
          </TouchableOpacity>
        </View>
        {/* <TextInput
          style={[styles.txtInput, {color: colors.primary}]}
          value={'Kategori ' + nama}
          placeholder="Kategori Laporan"
          editable={false}
        /> */}
        <TextInput
          style={styles.textArea}
          placeholderTextColor="#000"
          multiline={true}
          numberOfLines={5}
          placeholder="Isi Laporan"
          onChangeText={e => setDeskripsi(e)}
          value={deskripsi}
        />
        <TextInput
          style={styles.textArea}
          placeholder="Alamat Laporan"
          placeholderTextColor="#000"
          multiline={true}
          numberOfLines={2}
          onChangeText={e => setAlamat(e)}
          value={alamat}
        />

        {/* <TextInput style={styles.txtInput} value="" placeholder="Kelurahan" /> */}
        <View style={styles.textArea}>
          <Picker
            mode="dialog"
            style={styles.picker}
            itemStyle={{fontSize: 12}}
            selectedValue={kelurahan}
            onValueChange={(itemValue, itemIndex) => setKelurahan(itemValue)}>
            <Picker.Item label="Pilih Kelurahan" value="" />
            {/* <Picker.Item label="Kelurahan Tengah" value="0103" />
          <Picker.Item label="Kelurahan Melayu" value="0102" /> */}
            {kelurahanList.map(item => {
              return (
                <Picker.Item
                  key={item.kode_kelurahan}
                  label={item.nama_kelurahan}
                  value={item.kode_kelurahan}
                />
              );
            })}
          </Picker>
        </View>
        {/* <Text style={styles.btnText}>LAPOR</Text> */}
        <TouchableOpacity
          onPress={handleSubmit}
          style={[styles.btnUpload, {marginTop: 16, marginBottom: 20}]}>
          {isLoading ? (
            <>
              <Text style={styles.btnText}>Memproses Data</Text>
              <ActivityIndicator color="#fff" />
            </>
          ) : (
            <Text style={styles.btnText}>LAPOR</Text>
          )}
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    paddingHorizontal: 16,
  },
  imgWrapper: {
    marginTop: 6,
    width: '100%',
    height: 160,
    // backgroundColor: '#EFEFEF',
    borderRadius: 3,
  },
  btnUpload: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // height: 42,
    padding: 12,
    backgroundColor: colors.primary,
    borderRadius: 6,
    marginTop: 3,
  },
  btnText: {
    // fontFamily: 'Poppins-Reguler',
    fontWeight: '500',
    color: colors.white,
    marginLeft: 6,
    fontSize: 14,
  },
  txtInput: {
    marginTop: 10,
    // width: '100%',
    padding: 10,
    borderRadius: 6,
    borderColor: '#D6D6D6',
    borderWidth: 1,
    color: '#000',
  },
  textArea: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 10,
    paddingVertical: Platform.OS === 'ios' ? 16 : 10,
    paddingLeft: 10,
    borderRadius: 6,
    borderColor: '#D6D6D6',
    borderWidth: 2,
    textAlignVertical: Platform.OS === 'android' ? 'top' : 'center',
    color: '#000',
  },
  picker: {
    borderWidth: Platform.OS === 'android' ? 1 : null,
    width: '100%',
    padding: 10,
    borderRadius: 6,
    borderColor: '#D6D6D6',
    color: '#000',
  },
});
