/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  View,
  FlatList,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import colors from '../configs/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector, useDispatch} from 'react-redux';
import {setHome} from '../redux/actions';
import {SliderBox} from 'react-native-image-slider-box';
import axios from 'axios';
import url from '../configs/url';
import Label from '../components/Label';

export default function Home({navigation}) {
  const dispatch = useDispatch();
  // const [hoax, setHoax] = React.useState([]);
  const [data, setData] = React.useState([]);
  const HomeReducer = useSelector(state => state.HomeReducer);

  React.useEffect(() => {
    dispatch(setHome());
  }, [dispatch]);
  // React.useEffect(() => {
  //   let isMounted = true;
  //   if (isMounted) {
  //     axios
  //       .get(`${url.api}/laporan`, {headers: {token: 'secret123'}})
  //       .then(result => {
  //         console.log('get laporan success');
  //         setData(result.data.data);
  //       })
  //       .catch(err => console.log(err));
  //   }
  //   return () => {
  //     isMounted = false;
  //   };
  // }, []);
  // console.log(HomeReducer.slider);
  const fetchData = () => {
    axios
      .get(`${url.api}/laporan`, {headers: {token: 'secret123'}})
      .then(result => {
        console.log('get laporan success');
        setData(result.data.data);
      })
      .catch(err => console.log(err));
  };

  const [image, setImage] = React.useState([
    require('../assets/images/slider-default.jpg'),
  ]);

  React.useEffect(() => {
    let isMounted = true;
    // axios
    //   .get(`${url.api}/mobile/slider`, {headers: {token: 'secret123'}})
    //   .then(response => {
    //     let temp = [];
    //     response.data.data.map(item => {
    //       temp.push(item.image_path);
    //     });
    //     if (isMounted) {
    //       setImage(temp);
    //     }
    //   })
    //   .catch(err => console.log(err));
    // return () => {
    //   isMounted = false;
    // };
    let temp = [];
    HomeReducer.slider?.map(item => {
      temp.push(item.image_path);
    });
    setImage(temp);
  }, [HomeReducer]);

  // React.useEffect(() => {
  //   let isMounte = true;
  //   axios
  //     .get(`${url.api}/hoax`, {headers: {token: 'secret123'}})
  //     .then(response => {
  //       if (isMounte) {
  //         setHoax(response.data.data);
  //       }
  //     })
  //     .catch(err => console.log(err));

  //   return () => {
  //     isMounte = false;
  //   };
  // }, []);

  // React.useEffect(() => {
  //   let isMount = true;
  //   console.log('call version');
  //   axios
  //     .get(`${url.api}/setting/mobile-rilis`, {headers: {token: 'secret123'}})
  //     .then(response => {
  //       if (isMount) {
  //         version !== response.data.data.value ? setUpdate(true) : null;
  //       }
  //       console.log(update);
  //     })
  //     .catch(err => console.log(err));

  //   return () => {
  //     isMount = false;
  //   };
  // }, []);
  const footer = () => <View style={{height: 20}}></View>;

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.wraper}
      onPress={() =>
        navigation.navigate('LaporanDetail', {kode_laporan: item.kode_laporan})
      }>
      <Image
        style={styles.imgUtama}
        source={{
          uri: item.thumbnail,
        }}
      />
      <View style={styles.txtWrap}>
        <Label status={item.status_laporan} />
        <Text style={styles.txtJudul}>
          {item.deskripsi_laporan.length > 35
            ? item.deskripsi_laporan.substr(0, 35) + '...'
            : item.deskripsi_laporan}
        </Text>
        <Text style={styles.txtLokasi}>{item.kelurahan}</Text>
        <Text style={styles.txtLokasi}>{item.created_at}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.container}>
        <View style={styles.sliderWrapper}>
          <View style={styles.headerWrapper}>
            <Image
              source={require('../assets/images/logo-sm.png')}
              style={{height: 35}}
              resizeMode="contain"
            />
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    'https://www.facebook.com/Si-Hebat-107959294891809',
                  )
                }>
                <Icon
                  name="facebook"
                  size={22}
                  color={colors.primary}
                  style={{marginRight: 16}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://www.instagram.com/sihebat.id/')
                }>
                <Icon name="instagram" size={22} color={colors.primary} />
              </TouchableOpacity>
            </View>
          </View>
          <SliderBox
            images={image}
            autoplay
            circleLoop
            sliderBoxHeight={215}
            // resizeMethod={'resize'}
            resizeMode={'cover'}
            imageLoadingColor={colors.primary}
          />
        </View>
        <View style={styles.homeMenuWrapper}>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('Laporan')}>
              <Icon name="bullhorn" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>LAPORAN</Text>
          </View>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('HoaxList')}>
              <Icon name="ban" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>HOAX BUSTER</Text>
          </View>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('TelponPenting')}>
              <Icon
                name="volume-control-phone"
                size={30}
                color={colors.white}
              />
            </TouchableOpacity>
            <Text style={[styles.txtMenu, {fontSize: 11}]}>NO PENTING</Text>
          </View>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('Tempat')}>
              <Icon name="map-marker" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>TEMPAT</Text>
          </View>
        </View>
        <View style={styles.homeMenuWrapper}>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('Kuliner')}>
              <Icon name="cutlery" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>KULINER</Text>
          </View>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('Event')}>
              <Icon name="calendar-check-o" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>ACARA</Text>
          </View>
          <View style={styles.roundWrap}>
            <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('News')}>
              <Icon name="newspaper-o" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>BERITA</Text>
          </View>
          <View style={styles.roundWrap}>
            {/* <TouchableOpacity
              style={styles.roundBox}
              onPress={() => navigation.navigate('Pangan')}>
              <Icon name="shopping-basket" size={30} color={colors.white} />
            </TouchableOpacity>
            <Text style={styles.txtMenu}>HARGA PANGAN</Text> */}
          </View>
        </View>
        <View style={styles.headingWrapper}>
          <View style={styles.separator}></View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              flex: 1,
            }}>
            <Text style={styles.headingText}>LAPORAN WARGA</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Laporan')}>
              <Text style={{fontSize: 11, color: colors.accent}}>
                Selengkapnya
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <FlatList
          initialNumToRender={6}
          maxToRenderPerBatch={6}
          style={styles.postFlatList}
          data={hoax}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('HoaxDetail', {
                  idBerita: item.id,
                });
              }}>
              <View style={styles.postWrapper}>
                <View style={styles.postLeft}>
                  <View>
                    <Text style={styles.postTitle}>{item.judul}</Text>
                  </View>
                  <View style={styles.postMeta}>
                    
                    <View style={styles.postOpd}>
                      <Text style={styles.opdName}>#hoax #beritapalsu</Text>
                      <Text style={styles.time}>{item.updated_at}</Text>
                    </View>
                  </View>
                </View>
                <Image
                  source={{
                    uri: item.thumbnail,
                  }}
                  style={styles.postImage}
                />
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.id}
        /> */}
        {/* <ScrollView style={styles.flat}>
          {data.map(item => {
            return (
              <TouchableOpacity
                key={item.id}
                style={styles.wraper}
                onPress={() =>
                  navigation.navigate('LaporanDetail', {
                    kode_laporan: item.kode_laporan,
                  })
                }>
                <Image
                  style={styles.imgUtama}
                  source={{
                    uri: item.thumbnail,
                  }}
                />
                <View style={styles.txtWrap}>
                  <Label status={item.status_laporan} />
                  <Text style={styles.txtJudul}>
                    {item.deskripsi_laporan.length > 35
                      ? item.deskripsi_laporan.substr(0, 35) + '...'
                      : item.deskripsi_laporan}
                  </Text>
                  <Text style={styles.txtLokasi}>{item.kelurahan}</Text>
                  <Text style={styles.txtLokasi}>{item.created_at}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView> */}
        <FlatList
          style={styles.flat}
          data={HomeReducer.laporan}
          initialNumToRender={2}
          renderItem={renderItem}
          numColumns={2}
          keyExtractor={item => item.kode_laporan}
          ListFooterComponent={footer}></FlatList>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  roundWrap: {
    width: 81,
    alignItems: 'center',
    height: 68,
  },
  roundBox: {
    height: 62,
    width: 62,
    backgroundColor: colors.primary,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtMenu: {color: colors.primary, fontSize: 10, marginTop: 2.5},
  container: {
    flex: 1,
    backgroundColor: '#E8E5E5',
  },
  headerWrapper: {
    flexDirection: 'row',
    // backgroundColor: 'transparent',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 10,
    position: 'absolute',
    width: '100%',
    top: 0,
    left: 0,
    zIndex: 1000,
  },
  sliderWrapper: {
    backgroundColor: colors.white,
    height: 200,
    position: 'relative',
  },
  imageSlider: {
    width: 231,
    height: 130,
    marginRight: 8,
    resizeMode: 'cover',
  },
  homeMenuWrapper: {
    backgroundColor: colors.white,
    paddingHorizontal: 8,
    paddingVertical: 8,
    // flexWrap: 'wrap',
    // alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // marginTop: 8,
    height: 90,
  },
  boxMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: colors.primary,
    // width: 80,
    width: (Dimensions.get('window').width - 48) / 4,
    height: 45,
    borderRadius: 5,
    marginBottom: 6,
  },
  textWhite: {
    color: colors.white,
    fontSize: 11,
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
  },
  iconMenu: {
    marginLeft: 6,
    marginRight: 2,
  },
  headingWrapper: {
    backgroundColor: colors.white,
    paddingHorizontal: 16,
    paddingVertical: 8,
    alignItems: 'center',
    // justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 4,
  },
  separator: {
    backgroundColor: colors.primary,
    width: 5,
    height: 20,
    marginRight: 8,
  },
  headingText: {
    fontSize: 12,
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '700',
    color: '#000',
  },
  postFlatList: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 16,
  },
  postWrapper: {
    backgroundColor: colors.white,
    height: 120,
    borderTopWidth: 1,
    borderTopColor: colors.accent,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: Dimensions.get('window').width - 32,
  },
  postImage: {
    width: 160,
    height: 90,
    borderRadius: 6,
  },
  postLeft: {
    width: Dimensions.get('window').width - (32 + 160),
    justifyContent: 'space-around',
    // alignItems: 'center',
    height: 100,
    // backgroundColor: 'yellow',
  },
  postTitle: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  postMeta: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'yellow',
  },
  postOpd: {
    height: 20,
    justifyContent: 'space-around',
  },
  opdName: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 8,
    fontWeight: '600',
  },
  time: {
    // fontFamily: 'Poppins-Light',
    fontWeight: '400',
    fontSize: 8,
    marginRight: 8,
    color: colors.accent,
  },
  wraper: {
    width: (Dimensions.get('screen').width - 40) / 2,
    backgroundColor: '#fff',
    borderColor: '#000',
    marginBottom: 8,
    borderRadius: 6,
    overflow: 'hidden',
    marginRight: 8,
  },
  imgUtama: {
    height: 80,
    resizeMode: 'cover',
  },
  txtWrap: {
    padding: 12,
  },
  txtJudul: {
    textAlign: 'auto',
    fontWeight: '600',
    color: '#000',
  },
  txtLokasi: {
    fontWeight: '400',
    fontSize: 12,
    color: colors.accent,
  },
  flat: {
    flexWrap: 'wrap',
    paddingHorizontal: 16,
    paddingVertical: 8,
    flexDirection: 'row',
  },
});
