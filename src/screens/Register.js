/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';

// import PhoneInput from 'react-native-phone-number-input';

import TxtInput from '../components/TxtInput';
import ButtonPrimary from '../components/ButtonPrimary';
import colors from '../configs/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import url from '../configs/url';

export default function Register({navigation}) {
  const [value, setValue] = React.useState('');
  const [FB, setFB] = React.useState([]);
  // const [name, setName] = React.useState();
  const [password, setPassword] = React.useState('');
  // const [valid, setValid] = React.useState(false);
  // const [showMessage, setShowMessage] = React.useState(false);
  React.useEffect(() => {
    const getFB = async () => {
      try {
        let values = await AsyncStorage.getItem('fb_data');
        if (values != null) {
          values = JSON.parse(values);
          setFB(values);
        }
      } catch (error) {
        console.log(error);
      }
    };
    getFB();
  }, []);

  // const phoneInput = React.useRef(null);
  const [secure, setSecure] = React.useState(true);

  // ALL MEWTHOD
  // const saveToLocalStorage = async val => {
  //   try {
  //     val = JSON.stringify(val);
  //     await AsyncStorage.setItem('token', val);
  //     console.log('success storing auth token');
  //   } catch (error) {
  //     console.log(error + 'disini');
  //   }
  // };

  const submit = () => {
    let form_data = {
      fbid: FB.id,
      name: FB.name,
      email: FB.email,
      password: password,
      noHp: value,
    };
    if (password == '') {
      Alert.alert('Password tidak boleh kosong bosku.');
      return;
    }
    if (value == '') {
      Alert.alert('No Hape tidak boleh kosong bosku.');
      return;
    }
    axios
      .post(`${url.api}/register/`, form_data)
      .then(result => {
        console.log(result.data);
        // saveToLocalStorage(result.data.user_data.access_token);
        Alert.alert('Pendaftaran akun berhasil. Silahkan Login.');
        navigation.navigate('Login');
      })
      .catch(err => {
        Alert.alert('Gagal, mungkin email sudah terdaftar. ');
        console.log(err);
      });
  };
  const ikonMataBuka = (
    <TouchableOpacity onPress={() => setSecure(!secure)}>
      <Image source={require('../assets/images/icon-mata-tutup.png')} />
    </TouchableOpacity>
  );

  const ikonMataTutup = (
    <TouchableOpacity onPress={() => setSecure(!secure)}>
      <Image source={require('../assets/images/icon-mata-buka.png')} />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.heading}>DAFTAR</Text>
          <Text style={styles.subheading}>
            Mohon isi data yang sebenarnya, ini akan berguna di fitur Laporan.
          </Text>
          <TxtInput
            placeholder="Nama"
            keyboardType="email-address"
            value={FB.name}
            disable={false}
          />
          <TxtInput
            placeholder="Alamat Email"
            keyboardType="email-address"
            value={FB.email}
            disable={false}
          />
          <View style={styles.inputIcon}>
            <TextInput
              placeholder="Kata Sandi"
              placeholderTextColor={colors.primary}
              secureTextEntry={secure}
              onChangeText={e => setPassword(e)}
              style={{color: colors.accent}}
            />
            {secure ? ikonMataBuka : ikonMataTutup}
          </View>
          <TxtInput
            placeholder="Nomor Handphone"
            keyboardType="number-pad"
            value={value}
            onChangeText={e => setValue(e)}
          />

          <ButtonPrimary
            style={{marginTop: 20}}
            onPress={() => submit()}
            label="Daftar"
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: colors.accent}}>Sudah punya akun ? </Text>

            <Text
              onPress={() => navigation.navigate('Login')}
              style={{
                color: colors.primary,
              }}>
              Login
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 46,
    backgroundColor: '#fff',
    flex: 1,
  },
  heading: {
    fontSize: 30,
    color: colors.primary,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  subheading: {
    fontSize: 12,
    color: colors.accent,
    fontWeight: 'normal',
    marginBottom: 40,
  },
  inputText: {
    height: 44,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.accent,
    paddingHorizontal: 14,
    width: '100%',
    marginBottom: 20,
  },
  inputIcon: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 44,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.primary,
    paddingHorizontal: 14,
    marginBottom: 20,
    color: colors.primary,
  },
  inputNumber: {
    fontSize: 15,
    overflow: 'hidden',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.primary,
    marginTop: 20,
    marginBottom: 20,
    width: '100%',
  },
  inputDate: {
    display: 'flex',
    width: '100%',
    marginVertical: 20,
    height: 40,
    fontSize: 15,
  },
});
