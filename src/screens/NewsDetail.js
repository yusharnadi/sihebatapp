/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React from 'react';
import {
  Dimensions,
  Platform,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import ImageModal from 'react-native-image-modal';
import colors from '../configs/colors';
import url from '../configs/url';
import RenderHtml from 'react-native-render-html';
import moment from 'moment';
import Share from 'react-native-share';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function NewsDetail({navigation, route}) {
  var idLocale = require('moment/locale/id');
  moment.updateLocale('id', idLocale);
  const {idBerita} = route.params;
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const routeParam = `posts/${idBerita}`;
  const options = {
    title: data?.title?.rendered,
    url: data?.link,
    message: data?.title?.rendered,
  };

  const share = async (customOptions = options) => {
    try {
      await Share.open(customOptions);
    } catch (err) {
      console.log(err);
    }
  };

  const fetchData = id => {
    axios
      .get(`${url.mc + routeParam}`)
      .then(res => {
        setData(res.data);
        // console.log(res.data);
        setLoading(false);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    fetchData();
  }, []);
  const Loader = () => {
    return (
      <View
        style={[
          styles.container,
          {alignItems: 'center', justifyContent: 'center'},
        ]}>
        <ActivityIndicator size="large" />
      </View>
    );
  };
  if (loading) {
    return <Loader />;
  }
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        {/* <Image
          source={{
            uri: data.image,
          }}
          style={styles.imgUtama}
        /> */}
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{
            uri: data.better_featured_image?.source_url,
          }}
          imageBackgroundColor="#fff"
          style={{
            height: 200,
            width: Dimensions.get('screen').width,
          }}
        />
        <View style={styles.container}>
          <Text style={styles.txtJudul}>{data.title.rendered}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.txtDinas}>
              Media Center Kominfo Singkawang |{' '}
              {moment(data.date).format('dddd, DD/MM/YYYY  HH:mm:ss')}
            </Text>
          </View>
          {/* <Text style={styles.txtKonten}>{data.content.rendered}</Text> */}
          <RenderHtml
            source={{html: data.content.rendered}}
            contentWidth={Dimensions.get('window').width - 32}
            tagsStyles={tagsStyles}
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={async () => {
          await share();
        }}
        style={styles.TouchableOpacityStyle}>
        <Icon name="share-alt" size={22} color={colors.white} />
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  imgUtama: {
    height: 215,
    width: '100%',
  },
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#fff',
  },
  txtJudul: {
    fontSize: 18,
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    color: colors.primary,
  },
  txtDinas: {
    fontSize: 10,
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    color: colors.accent,
    fontStyle: 'italic',
  },
  txtKonten: {
    fontSize: 12,
    marginTop: 16,
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    textAlign: 'justify',
    color: '#000',
  },
  TouchableOpacityStyle: {
    //Here is the trick
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: colors.primary,
    borderRadius: 50,
  },
});
const tagsStyles = {
  body: {
    whiteSpace: 'normal',
    color: 'gray',
  },
  a: {
    color: 'green',
  },
  p: {
    color: '#000',
  },
};
