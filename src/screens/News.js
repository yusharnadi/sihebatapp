import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
  Platform,
  ActivityIndicator,
} from 'react-native';
import colors from '../configs/colors';
import axios from 'axios';
import url from '../configs/url';
import moment from 'moment';

export default function News({navigation}) {
  var idLocale = require('moment/locale/id');
  moment.updateLocale('id', idLocale);
  const [news, setNews] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [page, setPage] = React.useState(1);
  const [lastPage, setLastPage] = React.useState(false);

  React.useEffect(() => {
    const route = `posts?category=65&per_page=10&page=${page}`;
    axios
      .get(`${url.mc + route}`)
      .then(result => {
        if (result.data.code !== 'rest_post_invalid_page_number') {
          setNews(prevState => [...prevState, ...result.data]);
        } else {
          setLastPage(true);
        }

        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
        return null;
      });
  }, [page]);

  const Loader = () => {
    return (
      <View
        style={[
          styles.container,
          {alignItems: 'center', justifyContent: 'center'},
        ]}>
        <ActivityIndicator size="large" />
      </View>
    );
  };
  if (loading) {
    return <Loader />;
  }
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        initialNumToRender={10}
        maxToRenderPerBatch={10}
        onEndReached={() => {
          if (!loading && !lastPage) {
            setPage(prevPage => prevPage + 1);
          }
        }}
        style={styles.postFlatList}
        data={news}
        ListFooterComponent={Loader}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('NewsDetail', {
                idBerita: item.id,
              });
            }}>
            <View style={styles.postWrapper}>
              <View style={styles.postLeft}>
                <View>
                  <Text style={styles.postTitle}>{item.title?.rendered}</Text>
                </View>
                <View style={styles.postMeta}>
                  <Image
                    source={require('../assets/images/logo-skw-40.png')}
                    style={{height: 25}}
                    resizeMode="contain"
                  />
                  <View style={styles.postOpd}>
                    <Text style={styles.opdName}>
                      Berita Media Center Kominfo Singkawang
                    </Text>
                    <Text style={styles.time}>
                      {moment(item.date).format('dddd, DD/MM/YYYY  HH:mm:ss')}
                      {/* {moment(item.date).fromNow()} */}
                    </Text>
                  </View>
                </View>
              </View>
              <Image
                source={{
                  uri: item?.better_featured_image?.media_details?.sizes
                    ?.thumbnail.source_url,
                }}
                style={styles.postImage}
              />
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  postFlatList: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 16,
    paddingVertical: 8,
    marginBottom: 10,
  },
  postWrapper: {
    backgroundColor: colors.white,
    height: 120,
    borderBottomWidth: 1,
    borderBottomColor: colors.accent,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: Dimensions.get('window').width - 32,
  },
  postImage: {
    width: 108,
    height: 108,
    borderRadius: 8,
  },
  postLeft: {
    width: Dimensions.get('window').width - (32 + 110),
    justifyContent: 'space-around',
    // alignItems: 'center',
    height: 100,
  },
  postTitle: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  postMeta: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  postOpd: {
    height: 20,
    justifyContent: 'space-around',
  },
  opdName: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 8,
    fontWeight: '600',
    color: '#000',
  },
  time: {
    // fontFamily: 'Poppins-Light',
    fontWeight: '400',
    fontSize: 8,
    marginRight: 8,
    color: colors.accent,
  },
  inputText: {
    borderRadius: 25,
    borderColor: '#333',
    backgroundColor: '#fff',
    borderWidth: 1,
    marginHorizontal: 16,
    marginTop: 8,
    paddingLeft: 16,
    paddingVertical: 8,
    marginBottom: 4,
    color: '#000',
  },
});
