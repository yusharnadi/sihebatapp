/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Alert,
  ScrollView,
} from 'react-native';
import colors from '../configs/colors';
import TxtInput from '../components/TxtInput';
import Line from '../components/Line';
import ButtonPrimary from '../components/ButtonPrimary';
import ButtonFB from '../components/ButtonFB';
import axios from 'axios';
import url from '../configs/url';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch} from 'react-redux';
import {LoginManager, AccessToken} from 'react-native-fbsdk-next';

export default function Login({navigation}) {
  const [secure, setSecure] = React.useState(true);
  const [email, setEmail] = React.useState(null);
  const [password, setPassword] = React.useState(null);

  const dispatch = useDispatch();

  const saveToLocalStorage = async val => {
    try {
      val = JSON.stringify(val);
      await AsyncStorage.setItem('user_data', val);
      console.log('success storing auth token');
    } catch (error) {
      console.log(error + 'disini');
    }
  };
  const simpanFB = async val => {
    try {
      val = JSON.stringify(val);
      await AsyncStorage.setItem('fb_data', val);
      console.log('success storing FB DATA ');
    } catch (error) {
      console.log(error);
    }
  };

  const submitLogin = () => {
    const form_data = {
      email,
      password,
    };

    if (password === null) {
      Alert.alert('Password tidak boleh kosong bosku.');
      return;
    }
    if (email === null) {
      Alert.alert('Email tidak boleh kosong bosku.');
      return;
    }
    axios
      .post(`${url.api}/login`, form_data)
      .then(result => {
        console.log(result.data);
        saveToLocalStorage(result.data.user_data);
        dispatch({type: 'SET_AUTH', payload: result.data.user_data});
        navigation.navigate('Home', {screen: 'Home'});
      })
      .catch(err => {
        console.log('Axios : ' + err);
        Alert.alert(
          'Email atau Password Salah, Silahkan Masuk dengan Facebook jika anda lupa email dan password.',
        );
      });
  };
  const loginFb = () => {
    LoginManager.logOut();
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login Canceled' + JSON.stringify(result));
        } else {
          AccessToken.getCurrentAccessToken().then(async data => {
            const FBtoken = data.accessToken.toString();
            fetch(
              `https://graph.facebook.com/me?access_token=${FBtoken}&fields=id,name,picture,email,birthday`,
            )
              .then(response => response.json())
              .then(res => {
                console.log(res);
                simpanFB(res);
                axios
                  .post(
                    `${url.api}/loginfb`,
                    {email: res.email},
                    {headers: {token: 'secret123'}},
                  )
                  .then(respon => {
                    console.log(respon.data);
                    saveToLocalStorage(respon.data.user_data);
                    dispatch({
                      type: 'SET_AUTH',
                      payload: respon.data.user_data,
                    });
                    navigation.navigate('Home', {screen: 'Home'});
                  })
                  .catch(err => {
                    console.log('Axios : ' + err);
                    Alert.alert('Anda belum terdaftar, Silahkan daftar dulu.');
                    navigation.navigate('FBRegister');
                  });
              })
              .catch(error => {
                console.error('Error:', error);
              });
          });
        }
      },
      error => {
        console.log('Login Error' + error);
      },
    );
  };
  const ikonMataBuka = (
    <TouchableOpacity onPress={() => setSecure(!secure)}>
      <Icon name="eye" size={20} color={colors.primary} />
    </TouchableOpacity>
  );
  const ikonMataTutup = (
    <TouchableOpacity onPress={() => setSecure(!secure)}>
      <Icon name="eye-slash" size={20} color={colors.primary} />
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View style={styles.container}>
          <StatusBar BackgroundColor="white" />
          <Image
            style={styles.logo}
            source={require('../assets/images/logo-sm.png')}
          />
          <Text style={styles.subheading}>
            Silahkan Login Untuk Melanjutkan
          </Text>
          <TxtInput
            placeholder="Alamat Email"
            keyboardType="email-address"
            onChangeText={e => setEmail(e)}
          />
          <View style={styles.inputIcon}>
            <TextInput
              placeholder="Kata Sandi"
              secureTextEntry={secure}
              placeholderTextColor="#DA1F26"
              onChangeText={e => setPassword(e)}
              style={{color: colors.primary}}
            />
            {secure ? ikonMataBuka : ikonMataTutup}
          </View>
          <Text
            style={{
              color: colors.primary,
              alignSelf: 'flex-end',
              marginVertical: 20,
            }}
          />
          <ButtonPrimary onPress={() => submitLogin()} label="Masuk" />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Line />
            <Text style={{color: colors.accent, paddingHorizontal: 10}}>
              Or
            </Text>
            <Line />
          </View>
          <ButtonFB onPress={loginFb} label="Masuk Dengan Facebook" />
          {/* <View
          style={{
            marginVertical: 14,
            flexDirection: 'row',
            width: '60%',
            justifyContent: 'space-between',
          }}></View> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: colors.accent}}>Belum punya akun ? </Text>

            <Text
              onPress={() => navigation.navigate('FBRegister')}
              style={{
                color: colors.primary,
              }}>
              Daftar
            </Text>
          </View>
          <Text style={styles.ketentuan}>
            Dengan masuk ke aplikasi SI HEBAT, saya menyetujui segala Syarat dan
            Ketentuan SI HEBAT
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 46,
    backgroundColor: '#ffff',
    flex: 1,
  },
  logo: {
    marginBottom: 14,
  },
  logoText: {
    fontSize: 24,
    fontWeight: '700',
    color: colors.secondary,
    marginBottom: 40,
  },
  heading: {
    fontSize: 30,
    color: colors.secondary,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  subheading: {
    fontSize: 15,
    color: colors.accent,
    fontWeight: 'normal',
    marginBottom: 30,
  },
  inputText: {
    height: 44,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.primary,
    paddingHorizontal: 14,
    width: '100%',
    marginBottom: 20,
  },
  inputIcon: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 44,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.primary,
    paddingHorizontal: 14,
  },
  ketentuan: {
    color: colors.accent,
    fontSize: 12,
    marginTop: 20,
    textAlign: 'center',
  },
});
