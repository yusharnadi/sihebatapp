import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import colors from '../configs/colors';
import url from '../configs/url';

export default function TempatKategori({navigation, route}) {
  const {id_kategori} = route.params;

  const [data, setData] = React.useState([]);
  const [isLoading, setIsloading] = React.useState([]);

  const fetchData = () => {
    axios
      .get(`${url.api}/tempat/kategori/${id_kategori}`, {
        headers: {token: 'secret123'},
      })
      .then(result => {
        setData(result.data.data);
        console.log(result.data.data);
      })
      .catch(err => {
        setIsloading(false);
        console.log(err);
      });
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  const footer = () => <View style={{height: 32}}></View>;
  return (
    <SafeAreaView style={{flex: 1}}>
      <FlatList
        style={styles.tempatWrap}
        data={data}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={styles.tempat}
              onPress={() =>
                navigation.navigate('TempatDetail', {id: item.id})
              }>
              <Image
                source={{
                  uri: item.gambar_utama,
                }}
                style={styles.imgTempat}
              />
              <View
                style={{
                  justifyContent: 'space-around',
                  alignItems: 'flex-start',
                  flexWrap: 'wrap',
                  width: Dimensions.get('screen').width - 168,
                }}>
                <Text style={styles.txtTempat}>{item.nama_tempat}</Text>
                <Text style={styles.txtPopuler}>{item.alamat}</Text>
                <Text
                  style={[
                    styles.txtPopuler,
                    {color: colors.accent, fontStyle: 'italic'},
                  ]}>
                  {item.htm}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
        keyExtractor={item => item.id}
        ListFooterComponent={footer}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  tempatWrap: {
    backgroundColor: '#fff',
    padding: 16,
    paddingBottom: 52,
    flex: 1,
  },
  tempat: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  imgTempat: {
    height: 70,
    width: 120,
    resizeMode: 'cover',
    borderRadius: 8,
    marginRight: 16,
  },
  txtTempat: {
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  txtPopuler: {
    fontSize: 12,
    fontWeight: '600',
    color: '#000',
  },
});
