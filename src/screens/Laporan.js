import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Label from '../components/Label';
import colors from '../configs/colors';
import url from '../configs/url';

export default function Laporan({navigation}) {
  const [data, setData] = React.useState([]);

  React.useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    axios
      .get(`${url.api}/laporan`, {headers: {token: 'secret123'}})
      .then(result => {
        console.log('get laporan success');
        setData(result.data.data);
      })
      .catch(err => console.log(err));
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.wraper}
      onPress={() =>
        navigation.navigate('LaporanDetail', {kode_laporan: item.kode_laporan})
      }>
      <Image
        style={styles.imgUtama}
        source={{
          uri: item.thumbnail,
        }}
      />

      <View style={styles.txtWrap}>
        <Label status={item.status_laporan} />
        <Text style={styles.txtJudul}>
          {item.deskripsi_laporan.length > 35
            ? item.deskripsi_laporan.substr(0, 35) + '...'
            : item.deskripsi_laporan}
        </Text>
        <Text style={styles.txtLokasi}>{item.kelurahan}</Text>
        <Text style={styles.txtLokasi}>{item.created_at}</Text>
      </View>
    </TouchableOpacity>
  );
  const footer = () => <View style={{height: 20}}></View>;

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <FlatList
          style={styles.flat}
          data={data}
          renderItem={renderItem}
          numColumns={2}
          keyExtractor={item => item.kode_laporan}
          ListFooterComponent={footer}></FlatList>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  flat: {
    flexWrap: 'wrap',
    // paddingHorizontal: 16,
    paddingVertical: 8,
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  wraper: {
    width: (Dimensions.get('screen').width - 40) / 2,
    backgroundColor: '#fff',
    borderColor: '#000',
    marginBottom: 8,
    borderRadius: 6,
    overflow: 'hidden',
    marginRight: 8,
  },
  imgUtama: {
    height: 120,
    resizeMode: 'cover',
  },
  txtWrap: {
    padding: 12,
  },
  txtJudul: {
    textAlign: 'auto',
    fontWeight: '600',
    color: '#000',
  },
  txtLokasi: {
    fontWeight: '400',
    fontSize: 12,
    color: colors.accent,
  },
});
