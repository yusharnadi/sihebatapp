import React from 'react';
import {
  View,
  Text,
  Button,
  SafeAreaView,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import url from '../configs/url';
import colors from '../configs/colors';

const Profile = ({navigation}) => {
  const [fbData, setFbData] = React.useState({});
  const authData = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();
  const logout = () => {
    axios
      .get(`${url.api}/logoutall`, {
        headers: {
          Accept: 'aplication/json',
          Authorization: 'Bearer ' + authData.user_data.access_token,
        },
      })
      .then(() => {
        console.log('Server Logout success');

        // KEMUNGKINAN JIKA SESSION DI SERVER EXPIRED MAKA TIDAK BISA LOGIN
        AsyncStorage.multiRemove(['user_data', 'fb_data'])
          .then(data => {
            dispatch({type: 'SET_AUTH_LOGOUT'});
            console.log('Local storage was removed.');
          })
          .catch(err => console.log('Gagal remove localstorage ' + err));
      })
      .catch(err => console.log(err));
    AsyncStorage.multiRemove(['user_data', 'fb_data'])
      .then(data => {
        dispatch({type: 'SET_AUTH_LOGOUT'});
        console.log('Local storage was removed.');
      })
      .catch(err => console.log('Gagal remove localstorage ' + err));
  };

  React.useEffect(() => {
    // isLogin();
    AsyncStorage.getItem('fb_data', (err, res) => {
      if (res != null) {
        console.log(res);
        setFbData(JSON.parse(res));
      }
      if (err) {
        console.log(err);
      }
    });
  }, []);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.container}>
        <View style={styles.boxWhite}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={{
                uri: fbData?.picture?.data?.url,
              }}
              style={styles.img}
            />
            <View style={{marginLeft: 8}}>
              <Text style={styles.txtUsername}>{fbData.name}</Text>
              <Text style={styles.txtAccent}>{fbData.email}</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.boxWhite}
          onPress={() => {
            navigation.navigate('ResetPassword');
          }}>
          <Text style={{color: '#000'}}>Ubah Kata Sandi</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.boxWhite}
          onPress={() => {
            navigation.navigate('ResetHp');
          }}>
          <Text style={{color: '#000'}}>Ubah No Handphone</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.boxWhite}
          onPress={() => {
            navigation.navigate('LaporanSaya', {user_id: 1});
          }}>
          <Text style={{color: '#000'}}>Laporan Saya</Text>
        </TouchableOpacity>
        <Button
          onPress={() => logout()}
          title="Logout"
          color={colors.primary}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDDDDD',
  },
  boxWhite: {
    backgroundColor: '#fff',
    width: '100%',
    padding: 16,
    marginBottom: 8,
  },
  img: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  txtUsername: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '600',
    fontSize: 16,
    color: '#000',
  },
  txtAccent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    fontWeight: '400',
    fontSize: 12,
    color: colors.accent,
    // marginTop: 8,
  },
});

export default Profile;
