import axios from 'axios';
import React from 'react';
import {
  View,
  Platform,
  Text,
  Image,
  Dimensions,
  SafeAreaView,
  StyleSheet,
  ScrollView,
} from 'react-native';
import ImageModal from 'react-native-image-modal';
import Label from '../components/Label';
import colors from '../configs/colors';
import url from '../configs/url';

export default function KulinerDetail({navigation, route}) {
  const {kode_laporan} = route.params;
  const [data, setData] = React.useState([]);

  const fetchData = () => {
    axios
      .get(`${url.api}/kuliner/${kode_laporan}`, {
        headers: {token: 'secret123'},
      })
      .then(result => {
        console.log(result.data);
        setData(result.data.data);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        {/* <Image source={{uri: data.gambar}} style={styles.img} /> */}
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{uri: data.gambar}}
          imageBackgroundColor="#fff"
          style={{height: 300, width: Dimensions.get('screen').width}}
        />
        <View style={styles.container}>
          <View style={styles.boxWhite}>
            <Text style={styles.txtJudul}>{data.nama_kuliner}</Text>
          </View>
          <View style={styles.boxWhite}>
            <Text style={styles.txtHeading}>Deskripsi Kuliner</Text>
            <Text style={styles.txtContent}>{data.deskripsi}</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingHorizontal: 16,
    // paddingVertical: 8,
    backgroundColor: '#DDDDDD',
  },
  img: {
    height: 300,
    width: '100%',
    resizeMode: 'cover',
  },
  boxWhite: {
    backgroundColor: '#fff',
    width: '100%',
    padding: 16,
    marginBottom: 8,
  },
  txtJudul: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '700',
    fontSize: 18,
    color: '#000',
  },
  txtHeading: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '700',
    fontSize: 14,
    color: '#000',
  },
  txtAccent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    fontWeight: '400',
    fontSize: 10,
    color: colors.accent,
    marginTop: 8,
  },
  txtContent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '500',
    fontSize: 12,
    marginTop: 8,
    color: '#000',
  },
});
