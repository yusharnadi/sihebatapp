import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Linking,
  TouchableOpacity,
} from 'react-native';
import colors from '../configs/colors';

export default function Update() {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
      }}>
      <View>
        <Text style={{color: '#000', fontSize: 16}}>
          Mohon Update aplikasi anda terlebih dahulu.
        </Text>
        <TouchableOpacity
          style={{
            backgroundColor: colors.primary,
            height: 40,
            width: 100,
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            margin: 100,
          }}
          onPress={() =>
            Linking.openURL(
              'https://play.google.com/store/apps/details?id=com.sihebatapp',
            )
          }>
          <Text style={{color: '#fff', fontSize: 16}}>Update</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
