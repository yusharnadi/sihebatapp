/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../configs/colors';
import url from '../configs/url';

export default function Tempat({navigation}) {
  const [all, setAll] = React.useState([]);
  const [populer, setPopuler] = React.useState([]);
  const [kategori, setKategori] = React.useState([]);

  const [isLoading, setIsloading] = React.useState(false);

  const fetchAll = () => {
    setIsloading(true);
    axios
      .get(`${url.api}/tempat?limit=&status=`, {headers: {token: 'secret123'}})
      .then(result => {
        setAll(result.data.data);
      })
      .catch(err => {
        setIsloading(false);
        console.log(err);
      });
  };

  const fetchPopuler = () => {
    setIsloading(true);
    axios
      .get(`${url.api}/tempat?limit=&status=1`, {headers: {token: 'secret123'}})
      .then(result => {
        setPopuler(result.data.data);
      })
      .catch(err => {
        setIsloading(false);
        console.log(err);
      });
  };

  const fetchKategori = () => {
    setIsloading(true);
    axios
      .get(`${url.api}/kategori-tempat`, {headers: {token: 'secret123'}})
      .then(result => {
        setKategori(result.data.data);
      })
      .catch(err => {
        setIsloading(false);
        console.log(err);
      });
  };

  React.useEffect(() => {
    fetchPopuler();
    fetchKategori();
    fetchAll();
    console.log('effet fire');
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* <View style={styles.container}> */}
      <View style={styles.boxHeading}>
        <Text style={styles.txtHeading}>TEMPAT POPULER</Text>
        <Icon name="long-arrow-right" size={20} />
      </View>
      <ScrollView style={styles.populerWrap} horizontal={true}>
        {populer.map(item => {
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate('TempatDetail', {id: item.id})}
              style={styles.populerItem}
              key={item.id}>
              <Image
                style={styles.imgPopuler}
                source={{
                  uri: item.gambar_utama,
                }}
              />
              <Text style={styles.txtPopuler}>{item.nama_tempat}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>

      {/* KATEGORI SECTION */}

      <View style={styles.boxHeading}>
        <Text style={styles.txtHeading}>KATEGORI</Text>
        <Icon name="long-arrow-right" size={20} />
      </View>
      <ScrollView style={styles.kategoriWrap} horizontal={true}>
        {kategori.map(item => {
          return (
            <TouchableOpacity
              key={item.id_kategori}
              style={styles.kategoriItem}
              onPress={() =>
                navigation.navigate('TempatKategori', {
                  id_kategori: item.id_kategori,
                })
              }>
              <View style={styles.kategoriBox}>
                <Image source={{uri: item.icon}} style={styles.icon} />
              </View>
              <Text style={styles.txtIcon}>{item.nama_kategori}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>

      {/* ALL TEMPAT SECTION */}

      <FlatList
        style={styles.tempatWrap}
        data={all}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={styles.tempat}
              onPress={() =>
                navigation.navigate('TempatDetail', {id: item.id})
              }>
              <Image
                source={{
                  uri: item.gambar_utama,
                }}
                style={styles.imgTempat}
              />
              <View
                style={{
                  justifyContent: 'space-around',
                  alignItems: 'flex-start',
                  flexWrap: 'wrap',
                  width: Dimensions.get('screen').width - 168,
                }}>
                <Text style={styles.txtTempat}>{item.nama_tempat}</Text>
                <Text style={styles.txtPopuler}>{item.alamat}</Text>
                <Text
                  style={[
                    styles.txtPopuler,
                    {color: colors.accent, fontStyle: 'italic'},
                  ]}>
                  {item.htm}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
        keyExtractor={item => item.id}
      />
      {/* </View> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: '#DDDD',
  },
  boxHeading: {
    backgroundColor: '#fff',
    paddingVertical: 8,
    paddingHorizontal: 16,
    flexDirection: 'row',
  },
  populerWrap: {
    backgroundColor: '#fff',
    maxHeight: 125,
    width: '100%',
    paddingBottom: 8,
    paddingLeft: 16,
    marginBottom: 8,
    flex: 1,
  },
  populerItem: {
    width: 170,
    // height: 180,
    marginRight: 12,
  },
  imgPopuler: {
    height: 100,
    width: 170,
    borderRadius: 6,
    resizeMode: 'cover',
  },
  txtPopuler: {
    fontSize: 12,
    fontWeight: '600',
    color: '#000',
  },
  txtHeading: {
    fontSize: 14,
    fontWeight: '700',
    marginRight: 8,
    color: '#000',
  },
  kategoriWrap: {
    backgroundColor: '#fff',
    width: '100%',
    // paddingBottom: 8,
    paddingLeft: 16,
    marginBottom: 8,
    flexDirection: 'row',
    maxHeight: 100,
    // height: 100,
    flex: 1,
  },
  kategoriItem: {
    width: 70,
    marginRight: 12,
    alignItems: 'center',
    // justifyContent: 'center',
  },
  kategoriBox: {
    // width: 250,
    height: 70,
    width: 70,
    // marginRight: 12,
    borderWidth: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
  },
  icon: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  txtIcon: {
    fontSize: 9,
    fontWeight: '600',
    textAlign: 'center',
    color: '#000',
  },
  tempatWrap: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 4,
    paddingBottom: 32,
    flex: 1,
    // marginBottom: 16,
    // height: 300,
  },
  tempat: {
    flexDirection: 'row',
    marginBottom: 6,
  },
  imgTempat: {
    height: 70,
    width: 120,
    resizeMode: 'cover',
    borderRadius: 8,
    marginRight: 16,
  },
  txtTempat: {
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
});
