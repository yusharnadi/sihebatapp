import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Linking,
  ActivityIndicator,
} from 'react-native';
import colors from '../configs/colors';
import url from '../configs/url';

export default function TelponPenting() {
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const fetchData = () => {
    axios
      .get(`${url.api}/telepon`, {headers: {token: 'secret123'}})
      .then(result => {
        console.log(result.data.data);
        setData(result.data.data);
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
        return null;
      });
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  const Loader = () => {
    return (
      <View
        style={[
          styles.container,
          {alignItems: 'center', justifyContent: 'center'},
        ]}>
        <ActivityIndicator size="large" />
      </View>
    );
  };
  if (loading) {
    return <Loader />;
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {/* <TouchableOpacity
          style={[styles.row, {marginTop: 8}]}
          onPress={() => Linking.openURL('tel:+62562631798')}>
          <Text style={styles.txtItem}>RSUD ABDUL AZIZ</Text>
        </TouchableOpacity> */}
        {data.length > 0 &&
          data.map(item => {
            return (
              <TouchableOpacity
                key={item.id}
                style={[styles.row, {marginTop: 8}]}
                onPress={() => Linking.openURL(`tel:${item.no_telpon}`)}>
                <Text style={styles.txtItem}>{item.nama}</Text>
              </TouchableOpacity>
            );
          })}
        {/* <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+62562631008')}>
          <Text style={styles.txtItem}>RS SANTO VINCENTIUS</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+62562631791')}>
          <Text style={styles.txtItem}>RS HARAPAN BERSAMA</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+625624644000')}>
          <Text style={styles.txtItem}>RSIA WEMPE</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+628115661110')}>
          <Text style={styles.txtItem}>POLRES SINGKAWANG (MERPATI)</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+625624202652')}>
          <Text style={styles.txtItem}>BPKS SIAGA</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+625624211514')}>
          <Text style={styles.txtItem}>BPKS BHAKTI SUCI</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+62562637473')}>
          <Text style={styles.txtItem}>BPKS TUA PEKONG</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+6256263304')}>
          <Text style={styles.txtItem}>BPKS WIDYA BHAKTI</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+6282357548744')}>
          <Text style={styles.txtItem}>BPKS DWI TUNGGAL</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+62562635049')}>
          <Text style={styles.txtItem}>BPKS PASAR TURI</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+6282158537255')}>
          <Text style={styles.txtItem}>BPKS MANDIRI</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.row}
          onPress={() => Linking.openURL('tel:+625626301234')}>
          <Text style={styles.txtItem}>BPKS PASAR KULOR</Text>
        </TouchableOpacity> */}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 16,
    paddingHorizontal: 16,
  },
  row: {
    backgroundColor: colors.primary,
    padding: 16,
    borderRadius: 6,
    alignItems: 'center',
    marginBottom: 8,
  },
  txtItem: {
    fontWeight: '700',
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
  },
});
