import axios from 'axios';
import React from 'react';
import {
  Image,
  Dimensions,
  Platform,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
} from 'react-native';
import ImageModal from 'react-native-image-modal';
import colors from '../configs/colors';
import url from '../configs/url';

export default function BeritaDetail({navigation, route}) {
  const {idBerita} = route.params;
  const [data, setData] = React.useState([]);

  const fetchData = id => {
    axios
      .get(`${url.api}/beritas/${id}`, {headers: {token: 'secret123'}})
      .then(res => {
        setData(res.data.data);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    fetchData(idBerita);
  }, [idBerita]);
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        {/* <Image
          source={{
            uri: data.image,
          }}
          style={styles.imgUtama}
        /> */}
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{uri: data.image}}
          imageBackgroundColor="#fff"
          style={{
            height: 200,
            width: Dimensions.get('screen').width,
          }}
        />
        <View style={styles.container}>
          <Text style={styles.txtJudul}>{data.judul}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.txtDinas}>
              {data.namaOpd} | {data.updated_at}
            </Text>
          </View>
          <Text style={styles.txtKonten}>{data.konten}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  imgUtama: {
    height: 215,
    width: '100%',
  },
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#fff',
  },
  txtJudul: {
    fontSize: 18,
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    color: colors.primary,
  },
  txtDinas: {
    fontSize: 10,
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    color: colors.accent,
    fontStyle: 'italic',
  },
  txtKonten: {
    fontSize: 12,
    marginTop: 16,
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    textAlign: 'justify',
    color: '#000',
  },
});
