import axios from 'axios';
import React from 'react';
import {View, Text, ScrollView} from 'react-native';

export default function Alianyang() {
  const [pangan, setPangan] = React.useState([]);

  React.useEffect(() => {
    axios
      .get('https://videotron.digitaldev.id/api/display')
      .then(response => {
        if (response.data.info_pangan.length > 0) {
          let pangan_temp = response.data.info_pangan.filter(pangan => {
            return pangan.Market == 'Pasar Alianyang';
          });
          setPangan(pangan_temp);
        }
        // setPangan(response.data.info_pangan);
      })
      .catch(err => console.log(err));
    console.log(pangan.length);
  }, []);
  return (
    <ScrollView
      style={{backgroundColor: '#fff', flex: 1, paddingHorizontal: 16}}>
      {pangan.map((e, index) => {
        return (
          <View
            key={index}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
            }}>
            <Text style={{fontWeight: '400', fontSize: 18}}>{e.Commodity}</Text>
            <Text style={{fontWeight: '700', fontSize: 16}}>Rp. {e.Price}</Text>
          </View>
        );
      })}
    </ScrollView>
  );
}
