/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../configs/colors';
import url from '../configs/url';

export default function Event({navigation}) {
  const [data, setData] = React.useState([]);
  const [featured, setFeatured] = React.useState([]);

  const fetchData = () => {
    axios
      .get(`${url.api}/kiosk/event`, {headers: {token: 'secret123'}})
      .then(result => {
        setData(result.data.data);
      })
      .catch(err => console.log(err));

    axios
      .get(`${url.api}/kiosk/event?featured=1`, {headers: {token: 'secret123'}})
      .then(result => {
        setFeatured(result.data.data);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.boxWhite}>
          <Text style={styles.txtHeading}>ACARA POPULER</Text>
        </View>
        <ScrollView style={styles.sliderWrapper} horizontal={true}>
          {featured.map(ev => {
            return (
              <TouchableOpacity
                key={ev.id}
                onPress={() => {
                  navigation.navigate('EventDetail', {id: ev.id});
                }}>
                <Image
                  source={{
                    uri: ev.image_path,
                  }}
                  style={styles.imgSlider}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View style={styles.boxWhite}>
          <Text style={styles.txtHeading}>ACARA TERBARU</Text>
        </View>
        {/* <View style={styles.flatlist}> */}
        <FlatList
          style={styles.flatlist}
          data={data}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('EventDetail', {id: item.id});
                }}>
                <View style={styles.eventWrapper}>
                  {/* ITEM LOOP  */}
                  <Image
                    source={{
                      uri: item.image_path,
                    }}
                    style={styles.imgEvent}
                  />
                  <View
                    style={{
                      justifyContent: 'center',
                    }}>
                    <Text style={styles.txtJudul}>{item.name}</Text>
                    <View>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icon
                          name="map-marker"
                          size={15}
                          color={colors.primary}
                        />
                        <Text style={styles.txtDesc}>{item.tempat}</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Icon
                          name="calendar"
                          size={11}
                          color={colors.primary}
                        />
                        <Text style={styles.txtDesc}>{item.date}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={item => item.id}
        />
        {/* </View> */}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#DDDD',
    flex: 1,
  },
  boxWhite: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: '#fff',
  },
  sliderWrapper: {
    width: '100%',
    paddingBottom: 8,
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingLeft: 16,
    marginBottom: 8,
    height: 190,
  },
  imgSlider: {
    width: 195,
    height: 110,
    resizeMode: 'cover',
    borderRadius: 8,
    marginRight: 8,
  },
  flatlist: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: '#fff',
    // flex: 1,
  },
  eventWrapper: {
    flexDirection: 'row',
    marginBottom: 18,
  },
  imgEvent: {
    height: 60,
    width: 100,
    resizeMode: 'stretch',
    borderRadius: 6,
    marginRight: 8,
  },
  txtHeading: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
    fontWeight: '600',
    fontSize: 14,
    color: '#000',
  },
  txtJudul: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '700',
    fontSize: 14,
    color: '#000',
  },
  txtDesc: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    fontWeight: '300',
    fontSize: 12,
    marginLeft: 8,
    color: colors.accent,
  },
});
