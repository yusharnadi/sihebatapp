import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
  TextInput,
  Platform,
} from 'react-native';
import colors from '../configs/colors';
import {useSelector, useDispatch} from 'react-redux';
import {setBerita} from '../redux/actions';

export default function Berita({navigation}) {
  const [filter, setFilter] = React.useState([]);
  const [keyword, setKeyword] = React.useState('');
  const dispatch = useDispatch();

  const BeritaReducer = useSelector(state => state.BeritaReducer);

  React.useEffect(() => {
    dispatch(setBerita());
  }, [dispatch]);

  React.useEffect(() => {
    setFilter(BeritaReducer);
  }, [BeritaReducer]);

  const search = param => {
    setKeyword(param);
    let params = param.toLowerCase();
    let filteredData = BeritaReducer.filter(item => {
      return item.konten?.toLowerCase().includes(params);
    });
    setFilter(filteredData);
  };

  const SearchBar = () => {
    return (
      <TextInput
        placeholder="Cari Berita..."
        placeholderTextColor="#000"
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={search}
        value={keyword}
      />
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.inputText}
        placeholder="Cari Berita..."
        placeholderTextColor="#000"
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={search}
        value={keyword}
      />
      <FlatList
        initialNumToRender={6}
        // ListHeaderComponent={SearchBar}
        maxToRenderPerBatch={6}
        style={styles.postFlatList}
        data={filter}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('BeritaDetail', {
                idBerita: item.id,
              });
            }}>
            <View style={styles.postWrapper}>
              <View style={styles.postLeft}>
                <View>
                  <Text style={styles.postTitle}>{item.judul}</Text>
                </View>
                <View style={styles.postMeta}>
                  <Image
                    source={require('../assets/images/logo-skw-40.png')}
                    style={{height: 25}}
                    resizeMode="contain"
                  />
                  <View style={styles.postOpd}>
                    <Text style={styles.opdName}>{item.namaOpd}</Text>
                    <Text style={styles.time}>{item.updated_at}</Text>
                  </View>
                </View>
              </View>
              <Image
                source={{
                  uri: item.thumbnail,
                }}
                style={styles.postImage}
              />
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  postFlatList: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 16,
    paddingVertical: 8,
    marginBottom: 10,
  },
  postWrapper: {
    backgroundColor: colors.white,
    height: 120,
    borderBottomWidth: 1,
    borderBottomColor: colors.accent,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: Dimensions.get('window').width - 32,
  },
  postImage: {
    width: 108,
    height: 108,
    borderRadius: 8,
  },
  postLeft: {
    width: Dimensions.get('window').width - (32 + 108),
    justifyContent: 'space-around',
    // alignItems: 'center',
    height: 100,
  },
  postTitle: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  postMeta: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  postOpd: {
    height: 20,
    justifyContent: 'space-around',
  },
  opdName: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 8,
    fontWeight: '600',
    color: '#000',
  },
  time: {
    // fontFamily: 'Poppins-Light',
    fontWeight: '400',
    fontSize: 8,
    marginRight: 8,
    color: colors.accent,
  },
  inputText: {
    borderRadius: 25,
    borderColor: '#333',
    backgroundColor: '#fff',
    borderWidth: 1,
    marginHorizontal: 16,
    marginTop: 8,
    paddingLeft: 16,
    paddingVertical: 8,
    marginBottom: 4,
    color: '#000',
  },
});
