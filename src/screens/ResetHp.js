import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import colors from '../configs/colors';
import url from '../configs/url';

export default function ResetHp({navigation}) {
  const authData = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();
  const [hape, setHape] = React.useState('');
  const handleSubmit = () => {
    console.log(hape);
    if (hape === '') {
      Alert.alert('Tidak Boleh Kosong');
      return;
    }
    if (hape.length < 10) {
      Alert.alert('Minimal 11 Nomor');
      return;
    }

    axios
      .post(
        `${url.api}/resethp`,
        {noHp: hape},
        {
          headers: {Authorization: 'Bearer ' + authData.user_data.access_token},
        },
      )
      .then(result => {
        Alert.alert(result.data.message);
        setHape('');
        navigation.navigate('Profile');
      })
      .catch(err => {
        if (err.response) {
          if (err.response.status === 401) {
            AsyncStorage.multiRemove(['user_data', 'fb_data'])
              .then(data => {
                dispatch({type: 'SET_AUTH_LOGOUT'});
                console.log('Local storage was removed.');
              })
              .catch(erro => console.log('Gagal remove localstorage ' + erro));
          }
        }
        console.log(err);
        Alert.alert('Maaf, Gagal menghubungi server.');
      });
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainWrap}>
        <TextInput
          style={styles.txtInput}
          value={hape}
          placeholder="Masukan Nomor handphone"
          placeholderTextColor={colors.primary}
          onChangeText={e => setHape(e)}
        />
        <TouchableOpacity
          style={styles.btnPrimary}
          onPress={() => handleSubmit()}>
          <Text style={styles.txtBtn}>Simpan</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: 'white',
  },
  mainWrap: {
    padding: 16,
  },
  txtInput: {
    borderRadius: 6,
    borderWidth: 1,
    paddingLeft: 15,
    borderColor: colors.primary,
    fontWeight: '400',
    color: colors.primary,
    fontSize: 16,
    marginBottom: 16,
    height: 40,
  },
  btnPrimary: {
    borderRadius: 6,
    backgroundColor: colors.primary,
    paddingVertical: 12,
    alignItems: 'center',
  },
  txtBtn: {
    color: '#fff',
    fontSize: 18,
  },
  imgUtama: {
    height: 120,
    resizeMode: 'cover',
  },
  txtWrap: {
    padding: 12,
  },
  txtJudul: {
    textAlign: 'auto',
    fontWeight: '600',
    color: '#000',
  },
  txtLokasi: {
    fontWeight: '400',
    fontSize: 12,
    color: colors.accent,
  },
});
