import axios from 'axios';
import React from 'react';
import {
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Text,
  SafeAreaView,
  Image,
  View,
  Dimensions,
  Platform,
  TextInput,
} from 'react-native';
import colors from '../configs/colors';
import url from '../configs/url';

export default function Hoax({navigation}) {
  const [hoax, setHoax] = React.useState([]);
  const [filter, setFilter] = React.useState([]);
  const [keyword, setKeyword] = React.useState('');

  const search = param => {
    setKeyword(param);
    console.log('search');
    let params = param.toLowerCase();
    let filteredData = hoax.filter(item => {
      return item.konten?.toLowerCase().includes(params);
    });
    setFilter(filteredData);
  };

  React.useEffect(() => {
    let isMounte = true;
    axios
      .get(`${url.api}/hoax`, {headers: {token: 'secret123'}})
      .then(response => {
        if (isMounte) {
          setHoax(prevState => [...prevState, ...response.data.data]);
        }
      })
      .catch(err => console.log(err));

    return () => {
      isMounte = false;
    };
  }, []);

  React.useEffect(() => {
    setFilter(hoax);
  }, [hoax]);

  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.inputText}
        placeholder="Cari Berita..."
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={search}
        placeholderTextColor="#000"
        value={keyword}
      />
      <FlatList
        initialNumToRender={10}
        maxToRenderPerBatch={10}
        style={styles.postFlatList}
        data={filter}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('HoaxDetail', {
                idBerita: item.id,
              });
            }}>
            <View style={styles.postWrapper}>
              <View style={styles.postLeft}>
                <View>
                  <Text style={styles.postTitle}>{item.judul}</Text>
                </View>
                <View style={styles.postMeta}>
                  <View style={styles.postOpd}>
                    <Text style={styles.opdName}>#hoax #beritapalsu</Text>
                    <Text style={styles.time}>{item.updated_at}</Text>
                  </View>
                </View>
              </View>
              <Image
                source={{
                  uri: item.thumbnail,
                }}
                style={styles.postImage}
              />
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  postFlatList: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 16,
  },
  postWrapper: {
    backgroundColor: colors.white,
    height: 120,
    borderBottomWidth: 1,
    borderBottomColor: colors.accent,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: Dimensions.get('window').width - 32,
  },
  postImage: {
    width: 160,
    height: 90,
    borderRadius: 6,
  },
  postLeft: {
    width: Dimensions.get('window').width - (32 + 160),
    justifyContent: 'space-around',
    // alignItems: 'center',
    height: 100,
    // backgroundColor: 'yellow',
  },
  postTitle: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Medium' : null,
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  postMeta: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'yellow',
    color: '#000',
  },
  postOpd: {
    height: 20,
    justifyContent: 'space-around',
  },
  opdName: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 8,
    fontWeight: '600',
    color: '#000',
  },
  time: {
    // fontFamily: 'Poppins-Light',
    fontWeight: '400',
    fontSize: 8,
    marginRight: 8,
    color: colors.accent,
  },
  inputText: {
    borderRadius: 25,
    borderColor: '#333',
    backgroundColor: '#fff',
    borderWidth: 1,
    marginHorizontal: 16,
    marginTop: 8,
    paddingLeft: 16,
    paddingVertical: 8,
    marginBottom: 4,
    color: '#000',
  },
});
