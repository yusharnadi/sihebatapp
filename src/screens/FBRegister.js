/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image, Text, StyleSheet, SafeAreaView} from 'react-native';
import colors from '../configs/colors';
import ButtonFB from '../components/ButtonFB';
import {LoginManager, AccessToken} from 'react-native-fbsdk-next';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function FBRegister({navigation}) {
  const [FBToken, setFBToken] = React.useState('');
  const [FBData, setFBData] = React.useState(null);

  React.useEffect(() => {
    const GetFB = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem('fb_data');
        if (jsonValue != null) {
          navigation.navigate('Register');
          // console.log(jsonValue);
        }
      } catch (e) {
        console.log(e);
      }
    };
    GetFB();
  }, [FBData]);

  const simpanFB = async val => {
    try {
      val = JSON.stringify(val);
      await AsyncStorage.setItem('fb_data', val);
      console.log('success storing');
    } catch (error) {
      console.log(error);
    }
  };

  const login = () => {
    LoginManager.logOut();
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login Canceled' + JSON.stringify(result));
        } else {
          AccessToken.getCurrentAccessToken().then(async data => {
            const FBtoken = data.accessToken.toString();
            setFBToken(FBToken);
            // await saveStorage('FBtoken', FBtoken);
            // const userFbInfo = await get(
            //   `https://graph.facebook.com/me?access_token=${FBtoken}&fields=id,name,picture,email,friendlists,birthday`,
            // );
            // await saveStorage('userFbInfo', JSON.stringify(userFbInfo));
            // context.props.goMainPage();
            fetch(
              `https://graph.facebook.com/me?access_token=${FBtoken}&fields=id,name,picture,email,birthday`,
            )
              .then(response => response.json())
              .then(res => {
                setFBData(res);
                simpanFB(res);
              })
              .catch(error => {
                console.error('Error:', error);
              });
          });
        }
      },
      error => {
        console.log('Login Error' + error);
      },
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.rootContainer}>
        <Image source={require('../assets/images/logo-sm.png')} />
        <Text style={styles.txtTitle}>Registrasi Akun</Text>
        <ButtonFB label="Daftar dengan Akun Facebook" onPress={() => login()} />
        <View style={{flexDirection: 'row'}}>
          <Text style={{color: colors.accent}}>Sudah punya akun ? {''}</Text>
          <Text
            style={{color: colors.primary}}
            onPress={() => navigation.navigate('Login')}>
            Login disini.
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 16,
    paddingVertical: 32,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: colors.white,
  },
  txtTitle: {
    textTransform: 'uppercase',
    color: colors.primary,
    fontSize: 24,
    marginVertical: 20,
  },
});
