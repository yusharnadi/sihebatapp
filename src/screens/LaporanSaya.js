import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Label from '../components/Label';
import colors from '../configs/colors';
import url from '../configs/url';

export default function LaporanSaya({navigation}) {
  const [data, setData] = React.useState([]);
  const authData = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();
  console.log(authData);

  React.useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    axios
      .get(`${url.api}/laporan/user/${authData.user_data.id}`, {
        headers: {
          token: 'secret123',
          Authorization: 'Bearer ' + authData.user_data.access_token,
        },
      })
      .then(result => {
        console.log('get laporan success');
        setData(result.data.data);
      })
      .catch(err => {
        if (err.response) {
          if (err.response.status === 401) {
            AsyncStorage.multiRemove(['user_data', 'fb_data'])
              .then(data => {
                dispatch({type: 'SET_AUTH_LOGOUT'});
                console.log('Local storage was removed.');
              })
              .catch(err => console.log('Gagal remove localstorage ' + err));
          }
        } else {
          Alert.alert('Maaf, Error menghubungi server.');
        }
      });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.wraper}
      onPress={() =>
        navigation.navigate('LaporanDetail', {kode_laporan: item.kode_laporan})
      }>
      <Image
        style={styles.imgUtama}
        source={{
          uri: item.thumbnail,
        }}
      />
      <View style={styles.txtWrap}>
        <Label status={item.status_laporan} />
        <Text style={styles.txtJudul}>
          {item.deskripsi_laporan.length > 35
            ? item.deskripsi_laporan.substr(0, 35) + '...'
            : item.deskripsi_laporan}
        </Text>
        <Text style={styles.txtLokasi}>{item.kelurahan}</Text>
        <Text style={styles.txtLokasi}>{item.created_at}</Text>
      </View>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <FlatList
          style={styles.flat}
          data={data}
          renderItem={renderItem}
          numColumns={2}
          keyExtractor={item => item.kode_laporan}></FlatList>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: '#ddd',
  },
  flat: {
    flexWrap: 'wrap',
    // paddingHorizontal: 16,
    paddingVertical: 8,
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  wraper: {
    width: (Dimensions.get('screen').width - 40) / 2,
    backgroundColor: '#fff',
    borderColor: '#000',
    marginBottom: 8,
    borderRadius: 6,
    overflow: 'hidden',
    marginRight: 8,
  },
  imgUtama: {
    height: 120,
    resizeMode: 'cover',
  },
  txtWrap: {
    padding: 12,
  },
  txtJudul: {
    textAlign: 'auto',
    fontWeight: '600',
    color: '#000',
  },
  txtLokasi: {
    fontWeight: '400',
    fontSize: 12,
    color: colors.accent,
  },
});
