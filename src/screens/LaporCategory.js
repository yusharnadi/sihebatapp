import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';
import {useDispatch} from 'react-redux';
import url from '../configs/url';

export default function LaporCategory({navigation}) {
  const [kategori, setKategori] = React.useState([]);

  React.useEffect(() => {
    fetchKategori();
  }, []);

  const fetchKategori = () => {
    axios
      .get(`${url.api}/kategori-laporan`, {
        headers: {Accept: 'aplication/json', token: 'secret123'},
      })
      // .then(result => result.json())
      .then(response => {
        const data = response.data.data;
        setKategori(data);
      })
      .catch(err => console.log(err));
  };
  const dispatch = useDispatch();
  const SubmitCat = props => {
    // dispatch({type: 'SET_CAT_ID', payload: 'id'});
    navigation.navigate('LaporStack', {id: props.id, nama: props.nama});
  };

  const Category = props => {
    return (
      <TouchableOpacity onPress={() => SubmitCat(props)}>
        <View style={styles.item}>
          <View style={styles.boxIcon}>
            <Image
              source={{
                uri: props.icon,
              }}
              style={{width: 42, height: 42}}
              resizeMode="contain"
            />
          </View>
          <Text style={styles.textIcon}>{props.nama}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.container}>
        <Text style={styles.textTitle}>Pilih Kategori Laporan</Text>
        <View style={styles.catIconWrapper}>
          {kategori.map(item => {
            return (
              <Category
                key={item.id_kategori}
                id={item.id_kategori}
                nama={item.nama_kategori}
                icon={item.icon}
              />
            );
          })}
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    padding: 16,
  },
  catIconWrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item: {
    width: (Dimensions.get('screen').width - 64) / 4,
    height: 100,
    // marginHorizontal: 10,
    marginVertical: 10,
    alignItems: 'center',
  },
  boxIcon: {
    width: (Dimensions.get('screen').width - 64) / 4,
    // width: 70,
    height: 70,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#919191',
    alignItems: 'center',
    justifyContent: 'center',
    // margin: 10,
  },
  textTitle: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '600',
    fontSize: 16,
    marginBottom: 10,
    color: '#000',
  },
  textIcon: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
    color: '#000',
  },
});
