import axios from 'axios';
import React from 'react';
import {
  View,
  Platform,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Label from '../components/Label';
import colors from '../configs/colors';
import url from '../configs/url';
import ImageModal from 'react-native-image-modal';

export default function LaporanDetail({navigation, route}) {
  const {kode_laporan} = route.params;
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const fetchData = () => {
    axios
      .get(`${url.api}/laporan/${kode_laporan}`, {
        headers: {token: 'secret123'},
      })
      .then(result => {
        console.log(result.data);
        setData(result.data.data);
        setLoading(false);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  const Loader = () => {
    return (
      <View
        style={[
          styles.container,
          {alignItems: 'center', justifyContent: 'center'},
        ]}>
        <ActivityIndicator size="large" />
      </View>
    );
  };

  if (loading) {
    return <Loader />;
  }
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{uri: data.gambar_bukti}}
          imageBackgroundColor="#fff"
          style={{height: 300, width: Dimensions.get('screen').width}}
        />
        <View style={styles.container}>
          <View style={styles.boxWhite}>
            <Text style={styles.txtJudul}>{data.deskripsi_laporan}</Text>
          </View>
          <View style={styles.boxWhite}>
            <Text style={styles.txtHeading}>Detail Laporan</Text>
            <Text style={styles.txtAccent}>Waktu Laporan</Text>
            <Text style={styles.txtContent}>
              {data.created_at + ' ' + data.jam_created_at.split(' ')[1]}
            </Text>
            <Text style={styles.txtAccent}>Kategori</Text>
            <Text style={styles.txtContent}>{data.kategori}</Text>
            <Text style={styles.txtAccent}>Alamat</Text>
            <Text style={styles.txtContent}>{data.alamat_laporan}</Text>
            <Text style={styles.txtAccent}>Kelurahan</Text>
            <Text style={styles.txtContent}>{data.kelurahan}</Text>
            <Text style={styles.txtAccent}>Kecamatan</Text>
            <Text style={styles.txtContent}>{data.kecamatan}</Text>
          </View>
          <View style={styles.boxWhite}>
            <Text style={styles.txtHeading}>Status Laporan</Text>
            <Label status={data.status_laporan} />
          </View>
          {data.status_laporan === '2' && (
            <View style={styles.boxWhite}>
              <Text style={styles.txtHeading}>Diproses oleh :</Text>
              <Text style={styles.dinas}>{data.opd}</Text>
            </View>
          )}
          {data.status_laporan === '0' && (
            <View style={styles.boxWhite}>
              <Text style={styles.txtHeading}>Alasan Penolakan Laporan </Text>
              <Text style={styles.txtJudul}>{data.alasan_penolakan}</Text>
            </View>
          )}
        </View>
        {data.status_laporan === '3' && (
          <>
            <ImageModal
              resizeMode="cover"
              modalImageResizeMode="contain"
              source={{uri: data.gambar_selesai}}
              imageBackgroundColor="#fff"
              style={{height: 300, width: Dimensions.get('screen').width}}
              alt="Foto Selesai di tanggapi"
            />
            <View style={styles.container}>
              <View style={styles.boxWhite}>
                <Text style={styles.txtHeading}>
                  Laporan diselesaikan oleh :
                </Text>
                <Text style={styles.dinas}>{data.opd}</Text>
                <View style={{marginTop: 10}} />
                <Text style={styles.txtHeading}>
                  Tanggapan Terhadap Laporan :
                </Text>
                <Text style={styles.txtJudul}>{data.ulasan_laporan}</Text>
                <Text style={styles.txtJudul}>
                  Diselesaikan pada : {data.jam_updated_at}
                </Text>
              </View>
            </View>
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingHorizontal: 16,
    // paddingVertical: 8,
    backgroundColor: '#DDDDDD',
  },
  img: {
    height: 300,
    width: '100%',
    resizeMode: 'cover',
  },
  boxWhite: {
    backgroundColor: '#fff',
    width: '100%',
    padding: 16,
    marginBottom: 8,
  },
  txtJudul: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '600',
    fontSize: 16,
    color: '#000',
  },
  txtHeading: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '700',
    fontSize: 14,
    color: '#000',
  },
  txtAccent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    fontWeight: '400',
    fontSize: 10,
    color: colors.accent,
    marginTop: 8,
  },
  txtContent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '500',
    fontSize: 12,
    // marginTop: 8,
    color: '#000',
  },
  dinas: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '700',
    fontSize: 16,
    marginTop: 8,
    fontStyle: 'italic',
    color: '#000',
  },
});
