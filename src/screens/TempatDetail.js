import axios from 'axios';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import ImageModal from 'react-native-image-modal';
import colors from '../configs/colors';
import url from '../configs/url';

export default function TempatDetail({navigation, route}) {
  const {id} = route.params;

  const [data, setData] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(false);

  const fetchData = () => {
    setIsLoading(true);
    axios
      .get(`${url.api}/tempat/${id}`, {headers: {token: 'secret123'}})
      .then(result => {
        setData(result.data.data);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
        setIsLoading(false);
      });
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {/* <Image source={{uri: data.gambar_utama}} style={styles.imgUtama} /> */}
        <ImageModal
          resizeMode="cover"
          modalImageResizeMode="contain"
          source={{uri: data.gambar_utama}}
          imageBackgroundColor="#fff"
          style={{height: 200, width: Dimensions.get('screen').width}}
        />
        <View style={styles.boxWhite}>
          <Text style={[styles.txtJudul, {marginBottom: 8}]}>
            {data.nama_tempat}
          </Text>
          <Text style={styles.txtDesc}>{data.deskripsi}</Text>
        </View>
        <View style={styles.boxWhite}>
          <Text style={styles.txtHeading}>Alamat</Text>
          <Text style={styles.txtDesc}>{data.alamat}</Text>

          <Text style={styles.txtHeading}>Kontak</Text>
          <Text style={styles.txtDesc}>{data.kontak ? data.kontak : '-'}</Text>
          <Text style={styles.txtHeading}>HTM</Text>
          <Text style={styles.txtDesc}>{data.html ? data.htm : 'Gratis'}</Text>
          <Text style={styles.txtHeading}>Fasilitas</Text>
          {data.fasilitas?.map((item, index) => (
            <Text style={styles.txtAccent} key={index}>
              {item}
            </Text>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDDD',
  },
  imgUtama: {
    height: 200,
    resizeMode: 'cover',
  },
  boxWhite: {
    backgroundColor: '#fff',
    padding: 16,
    marginBottom: 8,
  },
  txtJudul: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '700',
    fontSize: 16,
    color: '#000',
  },
  txtHeading: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-SemiBold' : null,
    fontWeight: '700',
    fontSize: 14,
    color: '#000',
  },
  txtAccent: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Light' : null,
    fontWeight: '400',
    fontSize: 10,
    color: colors.accent,
    marginTop: 8,
  },
  txtDesc: {
    fontFamily: Platform.OS === 'android' ? 'Poppins-Regular' : null,
    fontWeight: '500',
    fontSize: 12,
    marginBottom: 8,
    color: '#000',
  },
});
