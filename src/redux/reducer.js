import {combineReducers} from 'redux';

const initialStateBerita = [];
const BeritaReducer = (state = initialStateBerita, action) => {
  if (action.type === 'SET_BERITA') {
    state = action.payload;
  }
  return state;
};

const initialStateHome = [];
const HomeReducer = (state = initialStateHome, action) => {
  if (action.type === 'SET_HOME') {
    state = action.payload;
    //   let temp = [];
    //   action.payload.slider.map(item => {
    //     temp.push(item.image_path);
    //   });
    //   state = {
    //     slider: temp,
    //     laporan: action.payload.laporan,
    //   };
  }
  return state;
};

const initialStateAuth = {
  isLogin: false,
  user_data: null,
};
const AuthReducer = (state = initialStateAuth, action) => {
  if (action.type === 'SET_AUTH') {
    state = {
      isLogin: true,
      user_data: action.payload,
    };
  }
  if (action.type === 'SET_AUTH_LOGOUT') {
    state = {
      isLogin: false,
      user_data: null,
    };
  }
  return state;
};

const reducer = combineReducers({BeritaReducer, AuthReducer, HomeReducer});
export default reducer;
