import axios from 'axios';
import url from '../../configs/url';

export const setBerita = () => {
  return dispatch => {
    axios
      .get(`${url.api}/beritas`, {
        headers: {Accept: 'aplication/json', token: 'secret123'},
      })
      .then(result => {
        const responseAPI = result.data.data;
        dispatch({type: 'SET_BERITA', payload: responseAPI});
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const setHome = () => {
  return dispatch => {
    axios
      .get(`${url.api}/home`, {
        headers: {Accept: 'aplication/json', token: 'secret123'},
      })
      .then(result => {
        const home = result.data.data;
        dispatch({type: 'SET_HOME', payload: home});
      })
      .catch(err => {
        console.log(err);
      });
  };
};
