export const berita = [
  {
    id: 3,
    judul:
      'WALI KOTA SINGKAWANG AUDIENSI DENGAN MENPAREKRAF, INI PESAN SANDIAGA',
    konten:
      'Wali Kota Singkawang, Tjhai Chui Mie beraudiensi dengan Menteri Pariwisata dan Ekonomi Kreatif RI, Sandiaga Uno di Jakarta, Selasa (15/6/2021). Dalam pertemuan tersebut Wali Kota dan Menteri Sandiaga membahas pengembangan pariwisata dan ekonomi kreatif di daerah masing-masing.\r\nPada kesempatan itu, Tjhai Chui Mie menyampaikan proposal terkait kawasan wisata mangrove di Kota Singkawang.\r\nSecara khusus, Menteri Sandiaga memberikan instruksi kepada Tjhai Chui Mie, dan meminta untuk menggelar event berskala internasional.\r\n“Untuk Singkawang, saya langsung menugaskan ke ibu, karena ibu ini adalah Wali Kota Kota Tertoleran se-Indonesia, sudah selayaknya punya event internasional. Karena kita punya Bhinekka Tunggal Ika, dan kita ingin buktikan, bahwa bangsa Indonesia adalah bangsa yang paling toleran. Jadi narasi-narasi perpecahan, kita lakukan kontra narasi, yaitu event internasional. Ya ini mungkin bisa dikemas baik, bekerja sama dengan dunia usaha. Idenya seperti apa, tapi alangkah hebatnya akalau kita bisa tunjukkan ke mata dunia toleransi Indonsia lewat festival internasional,” kata Sandi, seperti yang diunggah pada akun Instagramnya, Rabu (16/6/2021).\r\nMenanggapi itu, Kabid Pariwisata Dinas Pariwisata, Pemuda dan Olahraga Kota Singkawang, Novar Zulfian, mengatakan, pihak siap mendorong program Menteri Pariwisata di Kota Singkawang. “Kami siap, searah dengan instruksi Menteri Pariwisata, bersama kementerian mempersiapkan event internasional, yang mencerminkan kebersamaan dalam keberagaman di kota tertoleran,” kata Novar.\r\nIa menambahkan, ini menjadi tantangan bagi pihaknya, untuk memajukan Pariwisata di Kota Singkawang. “Terlebih pariwisata Singkawang sudah dilirik oleh para wisatawan dunia. Karena itu, Mas Menteri meng-highlight Singkawang sebagai Kota Tertoleran dalam Rakor di Jakarta,” ujarnya.',
    namaOpd: 'Dinas Pariwisata, Pemuda dan Olahraga',
    updated_at: '2021-06-28 21:52:07',
    thumbnail:
      'https://sihebat.digitaldev.id/uploads/berita/thumbnail/1624976159.jpg',
  },
  {
    id: 4,
    judul:
      'JULI 2021, JADWAL PERSIAPAN PENGADAAN LAHAN AKSES JALAN BANDARA DIMULAI',
    konten:
      'Pemerintah kota Singkawang melaksanakan rapat koordinasi pengadaan tanah untuk pembangunan jalan akses menuju bandara kota Singkawang dengan Pemerintah Provinsi Kalimantan Barat secara daring di ruang TCM kantor Wali Kota Singkawang, Selasa (22/6/2021).\r\n\r\nUntuk percepatan jalan akses masuk bandara kota Singkawang, Dinas PUPR kota Singkawang telah melaksanakan survei terhadap lokasi yang direncanakan sebagai jalan akses masuk bandara.\r\n\r\nWali kota Singkawang, Tjhai Chui Mie menyampaikan kabar baik bagi Pemerintah kota Singkawang, dimana Pemerintah Provinsi Kalimantan Barat telah memberikan penjadwalan persiapan pengadaan tanah dan akan mulai dikerjakan kembali pada awal bulan Juli 2021.\r\n\r\n“Kegembiraan yang didapat dari Pemerintah Provinsi, dimana kita sudah mendapat penjadwalan persiapan pengadaan tanah pada awal bulan Juli 2021. Jadi, hal-hal selanjutnya yang perlu dilakukan akan segera dilaksanakan.” ungkap Tjhai Chui Mie.\r\n\r\nIa mengatakan terdapat 62 orang pemilik tanah yang terkena pembangunan jalan akses menuju bandara kota Singkawang. Sebagai tindak lanjut, Tjhai Chui mengatakan nantinya akan dilaksanakan penandatanganan surat tanah sepanjang 10,5 km dan lebar 50 meter sebagai bentuk penyerahan lahan.\r\n\r\n“Dari 62 orang pemilik tanah yang terdampak, sudah sekitar 50 persen pemilik tanah yang menyetujui dan menyerahkan lahan untuk pembangunan jalan akses menuju bandara. Saya berharap masyarakat yang belum memberikan hibah lahan untuk mempertimbangkan, karena pembangunan infrastruktur jalan ini adalah investasi bersama.” ujarnya.\r\n\r\nTjhai Chui Mie mengapresiasi masyarakat kota Singkawang yang telah bersedia menyerahkan lahan kepada Pemerintah kota Singkawang untuk mendukung proses pembangunan lahan akses menuju bandara. Menurutntya, dukungan konkrit dari masyarakat membantu percepatan perwujudan pembangunan lahan akses menuju bandara.\r\n\r\n“Jika lahan untuk akses jalan menuju bandara belum siap, sampai kapanpun tidak akan bisa dibangun. Maka, itulah pentingnya pembebasan lahan ini. Setelah penyerahan lahan sudah rampung, maka kami akan melanjutkan ke tahap selanjutnya dengan mengajukan anggaran kepada Pemerintah Pusat.” ujarnya. *Td',
    namaOpd: 'Dinas Pekerjaan Umum dan Penataan Ruang',
    updated_at: '2021-06-28 21:44:37',
    thumbnail:
      'https://sihebat.digitaldev.id/uploads/berita/thumbnail/1624976316.jpg',
  },
  {
    id: 5,
    judul: 'WALI KOTA IMBAU MASYARAKAT UNTUK VAKSINASI COVID-19',
    konten:
      'Wali Kota Singkawang, Tjhai Chui Mie mengimbau kepada masyarakat Kota Singkawang yang belum divaksin, agar mendaftarkan diri untuk mendapatkan vaksinasi.\r\nHal itu disampaikan Wali Kota saat meninjau pelaksanaan vaksinasi Covid-19 di gedung Pavilion, Senin (21/6/2021).\r\n“Saya mengapresiasi masyarakat kota Singkawang yang mengikuti program vaksinasi ini. Vaksinasi ini dilaksanakan di dua tempat, yaitu Pavilion 78 dan Singkawang Grand Mall,” kata Tjhai Chui Mie.\r\nDengan vaksinasi, masyarakat sudah membantu pemerintah untuk mengatasi dan menekan angka penyebaran Covid-19 di kota Singkawang.\r\nIa mengatakan masyarakat yang telah divaksin memiliki resiko rendah jika terpapar Covid-19 pasca vaksinasi.\r\nSelaku ketua Satgas Covid-19, Tjhai Chui Mie beserta jajaran akan terus berupaya agar ketersediaan vaksin terus dipasok ke kota Singkawang. Ia mengatakan kota Singkawang mendapatkan pasokan vaksin dari Polres kota Singkawang sebanyak 2.000 dosis.\r\n“Polda Kalimantan Barat melalui Polres Singkawang terdapat sekitar 2.000 dosis vaksin yang akan dipergunakan. Kemudian dari pemerintah akan datang sekitar 220 vaksin lagi.” ujarnya.',
    namaOpd: 'Dinas Kesehatan dan Keluarga Berencana',
    updated_at: '2021-06-28 21:58:09',
    thumbnail:
      'https://sihebat.digitaldev.id/uploads/berita/thumbnail/1624976502.jpg',
  },
];
