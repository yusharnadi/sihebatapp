import React from 'react';
import Login from './screens/Login';
import Register from './screens/Register';
import FBRegister from './screens/FBRegister';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './screens/Home';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import colors from './configs/colors';
import Profile from './screens/Profile';
import LaporCategory from './screens/LaporCategory';
import Lapor from './screens/Lapor';
import {useSelector, useDispatch} from 'react-redux';
import Tempat from './screens/Tempat';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Laporan from './screens/Laporan';
import BeritaDetail from './screens/BeritaDetail';
import LaporanDetail from './screens/LaporanDetail';
import Event from './screens/Event';
import TempatKategori from './screens/TempatKategori';
import TempatDetail from './screens/TempatDetail';
import Kuliner from './screens/Kuliner';
import KulinerDetail from './screens/KulinerDetail';
import EventDetail from './screens/EventDetail';
import TelponPenting from './screens/TelponPenting';
import LaporanSaya from './screens/LaporanSaya';
import ResetPassword from './screens/ResetPassword';
import ResetHp from './screens/ResetHp';
import Berita from './screens/Berita';
import HoaxDetail from './screens/HoaxDetail';
import Hoax from './screens/Hoax';
import axios from 'axios';
import url from './configs/url';
import version from './configs/version';
import Update from './screens/Update';
import Pangan from './screens/Pangan';
import Beringin from './screens/Beringin';
import Alianyang from './screens/Alianyang';
import News from './screens/News';
import NewsDetail from './screens/NewsDetail';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerStyle: {
          backgroundColor: colors.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
        cardStyle: {backgroundColor: '#E7E6E4'},
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({current: {progress}}) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      presentation="modal">
      <Stack.Screen
        name="HomeStack"
        component={Home}
        options={{
          headerShown: false,
          title: '',
        }}
      />
      <Stack.Screen name="Tempat" component={Tempat} />
      <Stack.Screen
        name="TempatKategori"
        component={TempatKategori}
        options={{title: 'Tempat Kategori'}}
      />
      <Stack.Screen
        name="TempatDetail"
        component={TempatDetail}
        options={{title: 'Detail Tempat'}}
      />
      <Stack.Screen
        name="Kuliner"
        component={Kuliner}
        options={{title: 'Kuliner Khas'}}
      />
      <Stack.Screen
        name="KulinerDetail"
        component={KulinerDetail}
        options={{title: 'Detail Kuliner'}}
      />
      <Stack.Screen
        name="Laporan"
        component={Laporan}
        options={{title: 'Laporan Warga'}}
      />
      <Stack.Screen
        name="BeritaList"
        component={Berita}
        options={{title: 'Berita Terbaru'}}
      />
      <Stack.Screen
        name="HoaxList"
        component={Hoax}
        options={{title: 'Hoax Terbaru'}}
      />
      <Stack.Screen
        name="BeritaDetail"
        component={BeritaDetail}
        options={{title: 'Detail Berita'}}
      />
      <Stack.Screen
        name="HoaxDetail"
        component={HoaxDetail}
        options={{title: 'Detail Hoax'}}
      />
      <Stack.Screen name="Event" component={Event} options={{title: 'Acara'}} />
      <Stack.Screen
        name="LaporanDetail"
        component={LaporanDetail}
        options={{title: 'Detail Laporan'}}
      />
      <Stack.Screen
        name="EventDetail"
        component={EventDetail}
        options={{title: 'Detail Acara'}}
      />
      <Stack.Screen
        name="TelponPenting"
        component={TelponPenting}
        options={{title: 'Telepon Penting'}}
      />
      <Stack.Screen
        name="Pangan"
        component={Pangan}
        options={{title: 'Harga Pangan'}}
      />
      <Stack.Screen
        name="Beringin"
        component={Beringin}
        options={{title: 'Pasar Beringin'}}
      />
      <Stack.Screen
        name="Alianyang"
        component={Alianyang}
        options={{title: 'Pasar Alianyang'}}
      />
      <Stack.Screen
        name="NewsDetail"
        component={NewsDetail}
        options={{title: 'Detail Berita'}}
      />
      <Stack.Screen name="News" component={News} options={{title: 'Berita'}} />
    </Stack.Navigator>
  );
};

const LaporStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerStyle: {
          backgroundColor: colors.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
        cardStyle: {backgroundColor: 'transparent'},
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({current: {progress}}) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      presentation="modal">
      <Stack.Screen
        name="LaporCategory"
        component={LaporCategory}
        options={{
          headerShown: false,
          title: '',
        }}
      />
      <Stack.Screen
        name="LaporStack"
        component={Lapor}
        options={{
          headerShown: true,
          title: '',
        }}
      />
    </Stack.Navigator>
  );
};
const AkunStacknav = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerStyle: {
          backgroundColor: colors.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
        cardStyle: {backgroundColor: 'transparent'},
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({current: {progress}}) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      presentation="modal">
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="LaporanSaya"
        component={LaporanSaya}
        options={{title: 'Laporan Saya'}}
      />
      <Stack.Screen
        name="LaporanDetail"
        component={LaporanDetail}
        options={{title: 'Detail Laporan'}}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{title: 'Ubah Kata Sandi'}}
      />
      <Stack.Screen
        name="ResetHp"
        component={ResetHp}
        options={{title: 'Ubah Nomor Handphone'}}
      />
    </Stack.Navigator>
  );
};

const AuthStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {backgroundColor: 'transparent'},
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({current: {progress}}) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      presentation="modal">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="FBRegister" component={FBRegister} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
};

const AppNav = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused ? 'ios-home' : 'ios-home-outline';
          } else if (route.name === 'Lapor') {
            iconName = focused ? 'md-aperture' : 'md-aperture-outline';
          } else if (route.name === 'Akun') {
            iconName = focused ? 'ios-person' : 'ios-person';
          }
          // return (
          //   <MaterialCommunityIcons name="home" color={color} size={size} />
          // );
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'white',
        tabBarActiveBackgroundColor: '#C20000',
        tabBarInactiveBackgroundColor: '#DA1F26',
        tabBarShowLabel: true,
        tabBarLabelStyle: {marginBottom: 6, marginTop: -3},
      })}
      // tabBarOptions={{
      //   activeTintColor: 'white',
      //   inactiveTintColor: 'white',
      //   activeBackgroundColor: '#C20000',
      //   inactiveBackgroundColor: colors.primary,
      //   showLabel: false,
      // }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackNavigator}
        options={{headerShown: false, title: 'Beranda'}}
      />
      <Tab.Screen
        name="Lapor"
        component={LaporStackNavigator}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Akun"
        component={AkunStacknav}
        options={{title: 'Akun', headerShown: false}}
      />
    </Tab.Navigator>
  );
};

const AuthNav = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused ? 'ios-home' : 'ios-home-outline';
          } else if (route.name === 'Lapor') {
            iconName = focused ? 'md-aperture' : 'md-aperture-outline';
          } else if (route.name === 'Akun') {
            iconName = focused ? 'ios-person' : 'ios-person';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'white',
        tabBarActiveBackgroundColor: '#C20000',
        tabBarInactiveBackgroundColor: '#DA1F26',
        tabBarShowLabel: true,
        tabBarLabelStyle: {marginBottom: 6, marginTop: -3},
      })}
      // tabBarOptions={{
      //   activeTintColor: 'white',
      //   inactiveTintColor: 'white',
      //   activeBackgroundColor: '#C20000',
      //   inactiveBackgroundColor: colors.primary,
      //   showLabel: false,
      // }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackNavigator}
        options={{title: 'Beranda', headerShown: false}}
      />
      <Tab.Screen
        name="Lapor"
        component={AuthStackNavigator}
        options={{tabBarHideOnKeyboard: true, headerShown: false}}
      />
      <Tab.Screen
        name="Akun"
        component={AuthStackNavigator}
        options={{
          tabBarHideOnKeyboard: true,
          title: 'Akun',
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

export default function Router() {
  const dispatch = useDispatch();
  const authData = useSelector(state => state.AuthReducer);
  console.log(authData);

  const [update, setUpdate] = React.useState(false);

  React.useEffect(() => {
    let isMount = true;
    console.log('call version');
    axios
      .get(`${url.api}/setting/mobile-rilis`, {headers: {token: 'secret123'}})
      .then(response => {
        if (isMount) {
          version !== response.data.data.value ? setUpdate(true) : null;
        }
        // console.log(update);
      })
      .catch(err => console.log(err));

    return () => {
      isMount = false;
    };
  }, []);

  React.useEffect(() => {
    // isLogin();
    AsyncStorage.getItem('user_data', (err, res) => {
      if (res != null) {
        dispatch({type: 'SET_AUTH', payload: JSON.parse(res)});
      }
      if (err) {
        console.log(err);
      }
    });
  }, []);

  if (update) {
    return <Update />;
  }

  return (
    <NavigationContainer>
      {authData.isLogin ? <AppNav /> : <AuthNav />}
      {/* <AppNav /> */}
    </NavigationContainer>
  );
}
