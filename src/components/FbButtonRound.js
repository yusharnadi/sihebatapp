import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';

export default function FbButtonRound(props) {
  return (
    <TouchableOpacity style={styles.btnFb} onPress={props.onPress}>
      <Image source={require('../assets/images/icon-fb-white.png')} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnFb: {
    width: 46,
    height: 46,
    borderRadius: 46,
    backgroundColor: '#0041A8',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 2,
  },
});
