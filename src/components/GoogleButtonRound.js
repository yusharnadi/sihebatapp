import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';

export default function GoogleButtonRound(props) {
  return (
    <TouchableOpacity style={styles.btnFb} onPress={props.onPress}>
      <Image source={require('../assets/images/icon-google.png')} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnFb: {
    width: 46,
    height: 46,
    borderRadius: 46,
    backgroundColor: '#F2F8FF',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 2,
  },
});
