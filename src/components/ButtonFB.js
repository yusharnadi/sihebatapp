/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableHighlight, Text, StyleSheet, Image, View} from 'react-native';

export default function ButtonFB(props) {
  return (
    <TouchableHighlight style={styles.btnPrimary} onPress={props.onPress}>
      <View
        style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <Image source={require('../assets/images/icon-fb-white.png')} />
        <Text
          style={{
            color: '#fff',
            fontSize: 15,
            fontWeight: '700',
            marginLeft: 20,
          }}>
          {props.label}
        </Text>
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  btnPrimary: {
    height: 44,
    borderRadius: 10,
    paddingHorizontal: 14,
    width: '100%',
    marginBottom: 20,
    backgroundColor: '#0041A8',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
