/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableHighlight, Text, StyleSheet} from 'react-native';
import colors from '../configs/colors';

export default function ButtonPrimary(props) {
  return (
    <TouchableHighlight style={styles.btnPrimary} onPress={props.onPress}>
      <Text style={{color: '#fff', fontSize: 15, fontWeight: '700'}}>
        {props.label}
      </Text>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  btnPrimary: {
    height: 44,
    borderRadius: 10,
    paddingHorizontal: 14,
    width: '100%',
    marginBottom: 20,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
