import React from 'react';
import {TextInput, StyleSheet} from 'react-native';
import colors from '../configs/colors';

export default function TxtInput(props) {
  // const prop = props;
  return (
    <TextInput
      style={[
        styles.inputText,
        {color: !props.disable ? colors.accent : colors.primary},
      ]}
      placeholder={props.placeholder}
      keyboardType={props.keyboardType}
      placeholderTextColor="#DA1F26"
      value={props.value}
      editable={props.disable}
      onChangeText={props.onChangeText}
    />
  );
}

const styles = StyleSheet.create({
  inputText: {
    height: 44,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.primary,
    paddingHorizontal: 14,
    width: '100%',
    marginBottom: 20,
    fontSize: 15,
    // color: prop.disable ? '#ddd' : colors.primary,
  },
});
