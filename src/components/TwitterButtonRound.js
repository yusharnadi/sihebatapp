import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';

export default function TwitterButtonRound(props) {
  return (
    <TouchableOpacity style={styles.btnFb} onPress={props.onPress}>
      <Image source={require('../assets/images/icon-twitter-white.png')} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnFb: {
    width: 46,
    height: 46,
    borderRadius: 46,
    backgroundColor: '#42AAFF',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 2,
  },
});
