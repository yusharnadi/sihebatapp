/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text} from 'react-native';
const Label = props => {
  let bg = '#000';
  let label = 'Ditolak';
  if (props.status === '1') {
    bg = '#ffc107';
    label = 'Menunggu';
  } else if (props.status === '2') {
    bg = '#FC544A';
    label = 'Diproses';
  } else if (props.status === '3') {
    bg = '#28a745';
    label = 'Selesai';
  }
  return (
    <View
      style={{
        // paddingHorizontal: 8,
        width: 80,
        paddingVertical: 2,
        borderRadius: 16,
        backgroundColor: bg,
        alignItems: 'center',
      }}>
      <Text style={{color: '#fff', fontSize: 11, fontWeight: '800'}}>
        {label}
      </Text>
    </View>
  );
};

export default Label;
