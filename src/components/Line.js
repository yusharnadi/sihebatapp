import React from 'react';
import {View, StyleSheet} from 'react-native';
import colors from '../configs/colors';

export default function Line() {
  return <View style={styles.line} />;
}

const styles = StyleSheet.create({
  line: {
    flex: 1,
    height: 0.5,
    backgroundColor: colors.accent,
  },
});
