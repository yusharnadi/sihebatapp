const colors = {
  primary: '#DA1F26',
  secondary: '#222b45',
  accent: '#8f9bb3',
  white: '#FFFFFF',
};

export default colors;
